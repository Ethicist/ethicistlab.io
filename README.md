# [https://ethicist.site/](https://ethicist.site/)
[![License badge](https://raster.shields.io/badge/license-MIT-lightgrey.png "License badge")](https://choosealicense.com/licenses/mit/) [![pipeline status](https://gitlab.com/Ethicist/ethicist.gitlab.io/badges/master/pipeline.svg)](https://gitlab.com/Ethicist/ethicist.gitlab.io/-/commits/master) [![Netlify Status](https://api.netlify.com/api/v1/badges/1cd5ebcd-1b83-49a1-94eb-eba4f8d66db9/deploy-status)](https://app.netlify.com/sites/clever-llama-763bbc/deploys)


![Screenshot](screenshot.png?raw=true "Screenshot")

---

This repo contains the source code of [https://ethicist.gitlab.io/](https://ethicist.gitlab.io/), powered by [jekyll](http://jekyllrb.com/) with theme [LOFFER](https://github.com/FromEndWorld/LOFFER) by [FromEndWorld](https://github.com/FromEndWorld).

---

Here is some helpful solutions used in this project:

* Jekyll tags and categories plugin by [Roger Veciana i Rovira](https://github.com/rveciana):
[https://geoexamples.com/other/2015/06/04/Jekyll-tags-plugin-gh-pages.html](https://geoexamples.com/other/2015/06/04/Jekyll-tags-plugin-gh-pages.html)

* Jekyll Post Navigation Within a Category solution by Adam Clarkson
[https://ajclarkson.co.uk/blog/jekyll-category-post-navigation/](https://ajclarkson.co.uk/blog/jekyll-category-post-navigation/)

* How to give your Jekyll Site Structured Data for Search with JSON-LD
[http://aramzs.github.io/jekyll/schema-dot-org/2018/04/27/how-to-make-your-jekyll-site-structured.html](http://aramzs.github.io/jekyll/schema-dot-org/2018/04/27/how-to-make-your-jekyll-site-structured.html)
