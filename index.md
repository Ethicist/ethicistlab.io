---
layout: default
title: Заметки
permalink: /
pagination: 
  enabled: true
  category: blog
  permalink: /:num/
---

{% if site.posts.size == 0 %}
  <h2>Здесь пока ничего нет.</h2>
{% endif %}

<div class="posts">

  {% for post in paginator.posts %}
    <article class="post">
	
      <h1>
        <a href="{{ site.baseurl }}{{ post.url }}">{{ post.title }}</a>
      </h1>

      <div clsss="meta">
        
        <span class="author">
          {{ post.author }}
        </span>
            
		<span class="date" datetime="{{ post.date | date: '%Y-%m-%d' }}">
			{% assign m = post.date | date: "%-m" %}
			{{ post.date | date: "%-d"  }}
				{% case m %}
				  {% when '1' %}января
				  {% when '2' %}февраля
				  {% when '3' %}марта
				  {% when '4' %}апреля
				  {% when '5' %}мая
				  {% when '6' %}июня
				  {% when '7' %}июля
				  {% when '8' %}августа
				  {% when '9' %}сентября
				  {% when '10' %}октября
				  {% when '11' %}ноября
				  {% when '12' %}декабря
				{% endcase %}
			{{ post.date | date: "%Y"}}
		</span>

        <ul class="tag">
          {% for tag in post.tags %}
          <li>
            <a href="{{ site.url }}{{ site.baseurl }}/tags/{{ tag }}">
              #{{ tag }}
            </a>
          </li>
          {% endfor %}
        </ul>
      </div>
	  
	  <br/>
	  
      <a href="{{ site.baseurl }}{{ post.url }}" class="read-more">Подробнее ...</a>

    </article>
  {% endfor %}
</div>

<div class="pagination">
  {% if paginator.previous_page %}
    <span class="prev">
      <a href="{{ site.baseurl }}{{ paginator.previous_page_path }}" class="prev">
        ← сюда
      </a>
    </span>
  {% endif %}
  {% if paginator.next_page %}
    <span class="next">
      <a href="{{ site.baseurl}}{{ paginator.next_page_path }}" class="next">
        туда →
      </a>
    </span>
  {% endif %}
</div>