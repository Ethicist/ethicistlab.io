---
layout: post
author: Ethicist
title: Установка Android SDK из терминала
description: >
  Установка Android SDK из терминала в несколько строк. 
categories: blog
tags: >
  android sdk bash script tutorial linux
---

## Установка

```bash
$ cd /tmp/
$ curl -O 'http://dl.google.com/android/android-sdk_r24.0.2-linux.tgz'
$ sudo tar -xvzf android-sdk_r24.0.2-linux.tgz -C /usr/lib/
$ rm -f ./android-sdk_r24.0.2-linux.tgz
$ sudo chmod -R a+rw /usr/lib/android-sdk-linux/
$ cd /usr/lib/android-sdk-linux/tools/
$ sudo chmod a+x ./android
$ ./android
```