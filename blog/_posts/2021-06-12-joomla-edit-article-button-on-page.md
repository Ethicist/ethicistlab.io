---
layout: post
author: Ethicist
title: Joomla - Добавляем на сайт кнопку редактирования материала
description: >
  Кнопка редактирования материала отображающаяся на сайте
categories: blog
tags: >
  joomla flexicontent customizing
---

Иногда возникает необходимость в быстром переходе к редактированию материала, не используя поиск в панели администратора Joomla.
Особенно это актуально, если на сайте очень много материалов, поиск не очень помогает, а заказчик присылает ссылки на сайт, которые вам ничего не говорят.
В таком случае может помочь добавление кнопки редактирования материала, которая отображается только для администратора и которая недоступна обычным пользователям. 

### 1. Авторизация

Для начала нам понадобится авторизация пользователя на сайте. Если такой функции на сайте нет, то нужно её добавить, т.к. авторизоваться придётся не только
в панели администратора, но и на самом сайте. Для этого создаём новое меню, назовём его "Служебное меню" и добавляем в него один пункт. `Пользователи » Форма авторизации` или `Users » Login Form`, обзовём его `Вход` и пропишем в alias `hidden-login`. Потому что раз на сайте не предусмотрена авторизация, то попасть туда должны только те, кто знают. 

### 2. Проверка пользователя

В данном примере кнопка будет отображаться только пользователю с правами администратора. В директории темы добавляем код в `index.php`:

В начале:

```php
<?php
  defined('_JEXEC') or die;
  JHTML::_("jquery.framework", false);
  $app = JFactory::getApplication();
  
  $params = $app->getTemplate(true)->params;
  $view = $app->input->get('view');
  $user = JFactory::getUser();
  $user_groups = $user->groups;
  $isroot = $user->authorise('core.admin');
?>
```

### 3. Отображение кнопки и переход к редактированию материала

Где-нибудь до закрытия тега <body>:

```php
<?php
  if($isroot && $view == "item") :
    $item_id = JRequest::getVar('id');
?>

    <a type="button"
       class="button btn btn-light"
       id="btn-admin-edit-article"
       onclick="edit_article(<?php echo explode(':',$item_id,2)[0]; ?>);"
    >
      <span>Редактировать</span>
    </a>

    <script>
      function edit_article(id) {
        window.open("https://yourwebsite.com/administrator/index.php?option=com_flexicontent&task=items.edit&view=item&id=" + id, '_blank').focus();
      }
    </script>

<?php endif; ?>
```

Если на вашем сайте не используется FLEXIContent, <s>то это очень хорошо</s> то путь к редактированию у компомента com_content будет немного другим:

```javascript
window.open("https://yourwebsite.com/administrator/index.php?option=com_content&view=article&layout=edit&id=" + id, '_blank').focus();
```

И немного стилей для кнопки:

```css
#btn-admin-edit-article {
  z-index: 9999;
  height: 50px;
  position: fixed;
  right: 20px;
  bottom: 20px;
  display: flex;
  align-items: center;
  justify-content: center;
  box-shadow: 0 3px 7px 0 rgb(0 0 0 / 25%);
  color: black;
  text-shadow: none;
  background-color: white !important;
  background-image: linear-gradient(rgba(255,255,255,0.15),rgba(0,0,0,0.1)) !important;
}
```