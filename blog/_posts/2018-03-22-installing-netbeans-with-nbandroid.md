---
layout: post
author: Ethicist
title: "Installing NetBeans 7.4 with NBAndroid plugin"
description: >
  Installing NetBeans 7.4 with NBAndroid plugin	
categories: blog
tags: >
  netbeans ide nbandroid plugin tutorial linux bash
image:
  path: /nbandroid/nbandroid.jpg
  caption: "NBAndroid в IDE NetBeans 7.4"
  alt: nbandroid
---

{% include media-post-image.html %}

## Step 1
Download and install NetBeans 7.4
```bash
$ cd /tmp/
$ curl -O 'http://download.netbeans.org/netbeans/7.4/final/bundles/netbeans-7.4-linux.sh'
$ sudo ./netbeans-7.4-linux.sh --javahome /usr/lib/java/jdk1.7.0_51
```

## Step 2
Download nbandroid v13.12.26
```bash
$ curl -O 'http://nbandroid.org/downloads/nbandroid-13-12-26-101847.zip'
```

## Step 3
Extract archive
```bash
$ unzip ./nbandroid-13-12-26-101847.zip
```

## Step 4
Launch NetBeans 7.4 (In this example we've installed NetBeans into /opt/)
```bash
$ /bin/sh "/opt/netbeans-7.4/bin/netbeans"
```

## Step 5
Go to Tools ⇒ Plugins, then press [Check for updates] button.

## Step 6
Choose the "Available Plugins" tab, find "JUnit" plugin, then install it.
Don't restart IDE after installing this plugin. If it asks to restart,
сhoose "Restart IDE later" option and continue.

## Step 7
In "Plugins" window click to the "Downloaded" tab. Press [Add Plugins...]

## Step 8
Navigate to /tmp/nbandroid/, select all .nbm's and press "Open" button.
If you got some problems with plugin dependences, go to step #6 and try
to fix them. So, if everything is OK, hit the [Install] button below the
list, and follow the next installation instructions.

## Step 9
Apply all available updates for your plugins (Optional) -- go to "Updates"
tab, press [Install] and follow the instructions. Restart IDE after updating.

## Step 10
Enjoy!
