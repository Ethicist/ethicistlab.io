---
layout: post
author: Ethicist
title: Немного полезностей для кастомизации Bootstrap 4
description: >
  Кастомизация Bootstrap 4
categories: blog
tags: >
  bootstrap css customizing web codepen
---

## Cкрыть стрелку после ссылки выпадающего списка

```css
.dropdown-toggle::after {
   display: none;
  /* or opacity: 0; */
}
```

<p class="codepen" data-height="265" data-theme-id="light" data-default-tab="html,result" data-user="ethicist" data-slug-hash="MWKQwja" style="height: 265px; box-sizing: border-box; display: flex; align-items: center; justify-content: center; border: 2px solid; margin: 1em 0; padding: 1em;" data-pen-title="Remove dropdown toogler arrow">
  <span>See the Pen <a href="https://codepen.io/ethicist/pen/MWKQwja">
  Remove dropdown toogler arrow</a> by Captain Anonymous (<a href="https://codepen.io/ethicist">@ethicist</a>)
  on <a href="https://codepen.io">CodePen</a>.</span>
</p>


## Добавить уголок для выпадающего списка

```css
.dropdown-menu {
  margin-top: 10px;  
}
 
.dropdown-menu::before {
    border-bottom: 9px solid rgba(0, 0, 0, 0.2);
    border-left: 9px solid rgba(0, 0, 0, 0);
    border-right: 9px solid rgba(0, 0, 0, 0);
    content: "";
    display: inline-block;
    left: 5%;
    position: absolute;
    top: -8px;
}

.dropdown-menu::after {
    border-bottom: 8px solid white;
    border-left: 9px solid rgba(0, 0, 0, 0);
    border-right: 9px solid rgba(0, 0, 0, 0);
    content: "";
    display: inline-block;
    left: 5%;
    position: absolute;
    top: -7px;
}
```

<p class="codepen" data-height="265" data-theme-id="light" data-default-tab="css,result" data-user="ethicist" data-slug-hash="oNbEXBp" style="height: 265px; box-sizing: border-box; display: flex; align-items: center; justify-content: center; border: 2px solid; margin: 1em 0; padding: 1em;" data-pen-title="Dropdown menu arrow">
  <span>See the Pen <a href="https://codepen.io/ethicist/pen/oNbEXBp">
  Dropdown menu arrow</a> by Captain Anonymous (<a href="https://codepen.io/ethicist">@ethicist</a>)
  on <a href="https://codepen.io">CodePen</a>.</span>
</p>

## Заменить слеш на стрелку в breadcrumb

```css
.breadcrumb-item + .breadcrumb-item::before {
  color: #369;
  content: "→ ";
  padding: 0 5px;
}
```

<p class="codepen" data-height="265" data-theme-id="light" data-default-tab="html,result" data-user="ethicist" data-slug-hash="ZEQrGeO" style="height: 265px; box-sizing: border-box; display: flex; align-items: center; justify-content: center; border: 2px solid; margin: 1em 0; padding: 1em;" data-pen-title="Customizing breadcrumb">
  <span>See the Pen <a href="https://codepen.io/ethicist/pen/ZEQrGeO">
  Customizing breadcrumb</a> by Captain Anonymous (<a href="https://codepen.io/ethicist">@ethicist</a>)
  on <a href="https://codepen.io">CodePen</a>.</span>
</p>
    
## Избавляемся от фонового цвета автозаполняемого поля в Chrome

```css
input:-webkit-autofill,
input:-webkit-autofill:hover,
input:-webkit-autofill:focus,
input:-webkit-autofill:active {
  transition: background-color 5000s ease-in-out 0s;
}
```

<p class="codepen" data-height="265" data-theme-id="light" data-default-tab="html,result" data-user="ethicist" data-slug-hash="xxZYGrL" style="height: 265px; box-sizing: border-box; display: flex; align-items: center; justify-content: center; border: 2px solid; margin: 1em 0; padding: 1em;" data-pen-title="Remove Chrome autofill background color">
  <span>See the Pen <a href="https://codepen.io/ethicist/pen/xxZYGrL">
  Remove Chrome autofill background color</a> by Captain Anonymous (<a href="https://codepen.io/ethicist">@ethicist</a>)
  on <a href="https://codepen.io">CodePen</a>.</span>
</p>
    
## Изменить вид полосы прокрутки как для страницы, так и для textarea

```css
::-webkit-scrollbar {
  width: 17px;
  background: white;
}

::-webkit-scrollbar-thumb {
  border: none;
  background: linear-gradient(-134deg, #6161ee 0, #c471cc 100%);
  background-size: 18px;
  /*border-radius: 6px;*/
  /*border-top-left-radius: 6px;*/
  /*border-bottom-left-radius: 6px;*/
}

::-webkit-scrollbar-thumb:hover {
  background: linear-gradient(-134deg, #5050dd 0, #b360bb 100%);
}

::-webkit-scrollbar-track {
  background: #fafbfc;
  border: none;
}
```

<p class="codepen" data-height="265" data-theme-id="light" data-default-tab="html,result" data-user="ethicist" data-slug-hash="QWyQbqw" style="height: 265px; box-sizing: border-box; display: flex; align-items: center; justify-content: center; border: 2px solid; margin: 1em 0; padding: 1em;" data-pen-title="Customizing scrollbar">
  <span>See the Pen <a href="https://codepen.io/ethicist/pen/QWyQbqw">
  Customizing scrollbar</a> by Captain Anonymous (<a href="https://codepen.io/ethicist">@ethicist</a>)
  on <a href="https://codepen.io">CodePen</a>.</span>
</p>

## Изменить вид выделения текста 

```css
::selection {
  background: #6161ee;
  color: white;
}

::-moz-selection {
  background: #6161ee;
  color: white;
}

::-webkit-selection {
  background: #6161ee;
  color: white;
}
```

<p class="codepen" data-height="445" data-theme-id="light" data-default-tab="html,result" data-user="ethicist" data-slug-hash="ExPQjEv" style="height: 445px; box-sizing: border-box; display: flex; align-items: center; justify-content: center; border: 2px solid; margin: 1em 0; padding: 1em;" data-pen-title="Customizing selection">
  <span>See the Pen <a href="https://codepen.io/ethicist/pen/ExPQjEv">
  Customizing selection</a> by Captain Anonymous (<a href="https://codepen.io/ethicist">@ethicist</a>)
  on <a href="https://codepen.io">CodePen</a>.</span>
</p>


## Изменить аттрибуты placeholder

```css
::placeholder {
  /* Chrome, Firefox, Opera, Safari 10.1+ */
  color: rgb(153, 153, 153) !important;
  opacity: 1; /* Firefox */
}

:-ms-input-placeholder {
  /* Internet Explorer 10-11 */
  color: rgb(153, 153, 153) !important;
}
```

<p class="codepen" data-height="265" data-theme-id="light" data-default-tab="html,result" data-user="ethicist" data-slug-hash="YzweXvJ" style="height: 265px; box-sizing: border-box; display: flex; align-items: center; justify-content: center; border: 2px solid; margin: 1em 0; padding: 1em;" data-pen-title="Customizing placeholder">
  <span>See the Pen <a href="https://codepen.io/ethicist/pen/YzweXvJ">
  Customizing placeholder</a> by Captain Anonymous (<a href="https://codepen.io/ethicist">@ethicist</a>)
  on <a href="https://codepen.io">CodePen</a>.</span>
</p>

<script async src="https://static.codepen.io/assets/embed/ei.js"></script>
