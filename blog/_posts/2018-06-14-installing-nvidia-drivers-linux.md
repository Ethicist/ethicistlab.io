---
layout: post
author: Ethicist
title: Установка и удаление драйверов NVIDIA в Linux
description: >
  Установка и удаление драйверов NVIDIA 
categories: blog
tags: >
  nvidia driver install uninstall tutorial linux
---

## Установка

Прежде всего узнаём модель видеокарты. После чего на [официальном сайте](https://nvidia.com/Download/index.aspx) подбираем необходимую версию, платформу и разрядность. В данном примере модель видеокарты может устареть, или же может выйти более новая версия драйверов. 

Загружаем установщик с оф. сайта:

```bash
# Для x64
$ wget http://us.download.nvidia.com/XFree86/Linux-x86_64/331.79/NVIDIA-Linux-x86_64-331.79.run

# Для x32
$ wget http://us.download.nvidia.com/XFree86/Linux-x86/331.79/NVIDIA-Linux-x86-331.79.run
```

Устанавливаем права на выполнение и запускаем установщик:

```bash
$ chmod +x ./NVIDIA-Linux-x86_64-331.79.run
$ ./NVIDIA-Linux-x86_64-331.79.run
```

## Удаление драйвера с помощью загруженного установщика

```bash
$ ./NVIDIA-Linux-x86_64-331.79.run --uninstall
```

Это было слишком просто. Можно и по-другому. <br>

Для начала вычищаем все конфиги драйвера:

```bash
$ sudo nvidia-settings --uninstall
```

Находим все пакеты, связанные с nvidia, которые gjтребуется удалить

```bash
$ dpkg -l | grep -i nvidia
```

Полученный список пакетов копируем и подставляем в команду ниже, вместо "package_nameы"

```bash
$ sudo apt-get remove --purge package_nameы
```

Обычно после этого нужно перейти к переустановке драйверов nouveau.

## Переустановка nouveau

```bash
$ sudo apt-get remove --purge xserver-xorg-video-nouveau xserver-xorg-video-nv
$ sudo apt-get install xserver-xorg-video-nouveau
$ sudo apt-get install --reinstall libgl1-mesa-glx libgl1-mesa-dri xserver-xorg-core
```

Обновляем конфиги xorg

```bash
$ sudo dpkg-reconfigure xserver-xorg
```

Перезагружаемся

```bash
$ sudo reboot
```