---
layout: post
author: Ethicist
title: Установка Opera Presto 12.16 на Linux
description: >
  Минималистичное расписание звонков
categories: blog
tags: >
  opera presto bash script tutorial linux debian
image:
  path: /opera-presto/opera.jpg
  caption: "Не нашёл скриншота получше. Здесь что-то пошло не так."
  alt: opera
---

{% include media-post-image.html %}

## Установка

Создаём временную директорию:

```bash
$ mkdir temp
$ cd temp
```

Загружаем пакет, устанавливаем, после чего удаляем .deb.

```bash
$ curl -O https://ftp.opera.com/pub/opera/linux/1216/opera_12.16.1860_i386.deb
$ sudo dpkg -i ./opera_12.16.1860_i386.deb
$ rm ./opera_12.16.1860_i386.deb
```

Пробуем запустить, и... Нет.

<center>
{%
	include media-image.html
	url="/opera-presto/opera-gstreamer.jpg"
	caption="GStreamer. Неразрешённая зависимость."
%}
</center>

Начинаем исправлять зависимости:

libicu

```bash
$ curl -O http://security.ubuntu.com/ubuntu/pool/main/i/icu/libicu55_55.1-7ubuntu0.3_i386.deb
$ sudo dpkg -i ./libicu55_55.1-7ubuntu0.3_i386.deb
```

libxml2

```bash
$ curl -O http://security.ubuntu.com/ubuntu/pool/main/libx/libxml2/libxml2_2.9.3+dfsg1-1ubuntu0.5_i386.deb
$ sudo dpkg -i ./libxml2_2.9.3+dfsg1-1ubuntu0.5_i386.deb
```

Самое весёлое: libgstreamer 0.10

```bash
$ curl -O http://mirrors.edge.kernel.org/ubuntu/pool/universe/g/gstreamer0.10/libgstreamer0.10-0_0.10.36-1.5ubuntu1_i386.deb
$ sudo dpkg -i ./libgstreamer0.10-0_0.10.36-1.5ubuntu1_i386.deb

$ curl -O http://security.ubuntu.com/ubuntu/pool/universe/g/gst-plugins-good0.10/gstreamer0.10-plugins-good_0.10.31-3+nmu4ubuntu2.16.04.3_i386.deb
$ sudo dpkg -i ./gstreamer0.10-plugins-good_0.10.31-3+nmu4ubuntu2.16.04.3_i386.deb

$ curl -O http://security.ubuntu.com/ubuntu/pool/universe/g/gst-plugins-base0.10/gstreamer0.10-plugins-base_0.10.36-2ubuntu0.1_i386.deb
$ sudo dpkg -i ./gstreamer0.10-plugins-base_0.10.36-2ubuntu0.1_i386.deb

$ curl -O http://security.ubuntu.com/ubuntu/pool/universe/g/gst-plugins-base0.10/libgstreamer-plugins-base0.10-0_0.10.36-2ubuntu0.1_i386.deb
$ sudo dpkg -i ./libgstreamer-plugins-base0.10-0_0.10.36-2ubuntu0.1_i386.deb
```

Обновляемся, перепроверяем неразрешённые зависимости. Выявляется libxml2. Ставим.

```bash
$ sudo apt update
$ sudo app -f install
$ sudo apt install libxml2
```

Запускаем.

```bash
$ opera
```