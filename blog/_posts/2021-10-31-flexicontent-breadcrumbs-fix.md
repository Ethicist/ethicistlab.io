---
layout: post
author: Ethicist
title: FLEXIContent - Исправление ссылок в breadcrumbs
description: >
  Что делать если при использовании FLEXIContent в breadcrumbs отображаются неверные ссылки?
categories: blog
tags: >
  joomla flexicontent
---

### Проблема
Joomla 3, FLEXIContent 3.3.9. На сайте используется модуль Breadcrumbs.
Первая и последняя ссылки в порядке, но остальные ведут не туда, куда нужно, а если точнее у их адресов некорректная маршрутизация.

### Решение

Кратко: исправляем маршрутизацию в `/components/com_flexicontent/helpers/route.php`

Подробно:

В `/components/com_flexicontent/helpers/route.php`, строка 1189

Находим:
```php
    static $view_varnames = array(FLEXI_ITEMVIEW=>'id', 'category'=>'cid', 'tags'=>'id', 'flexicontent'=>'rootcatid');
```

Заменяем на:
```php
    static $view_varnames = array(FLEXI_ITEMVIEW=>'id', 'category'=>'cid', 'tags'=>'id', 'flexicontent'=>'rootcat');
```

В `/components/com_flexicontent/helpers/route.php` начиная со строки 638

Находим:

```php
    $link = str_replace('view=category', 'view=flexicontent', $link);
    $link = str_replace('cid=', 'rootcat=', $link);
```

Заменяем на:
```php
    $link = str_replace('view=category', 'view=flexicontent', $link);
    $link = str_replace('cid=', 'rootcat=', $link);
    $str = strpos($link, ":");
    $link = substr($link, 0, $str);
    $link .= '&Itemid='.$Itemid;
```

Актуально для FLEXIContent версии 3.3.9

Источник:
https://www.flexicontent.org/forum/21-troubleshooting/55206-wrong-url-in-breadcrumb.html