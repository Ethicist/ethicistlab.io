---
layout: post
author: Ethicist
title: FLEXIcontent - "failed to open output stream. temporary path not writable"
description: >
  Что делать если при загрузке изображений в материал flexicontent появляется Alert с ошибкой
categories: blog
tags: >
  joomla flexicontent
---

### Проблема
У нас установлена CMS Joomla и система управления контентом FLEXIcontent.
При загрузке изображения в галерею материала файл не сохраняется на сервере и появляется Alert с сообщением 
`failed to open output stream. temporary path not writable`.

### Решение

Кратко: проверить и исправить пути в `configuration.php` Joomla.

Подробно:

Для начала нам нужно найти абсолютный путь ко временной директории сайта. <BR/> Для этого создадим скрипт `path.php`

```php
<?php
  $ini_path = php_ini_loaded_file();

  if ($ini_path) {
    echo 'php.ini: ' . $ini_path;
  } else {
    echo 'A php.ini file was not loaded';
  }

  $path = getcwd();
  echo 'Your absoluthe path is: ' . $path;
?>
```

Если такой возможности нет, то можно посмотреть настройки PHP в панели управления хостингом, или же вызвать где-нибудь в коде
```php
<?php phpinfo(); ?>
```

В зависимости от сервера, пути могут различаться. В моём случае это `/var/www/vhosts/sitename.com/httpdocs/logs` и `/var/www/vhosts/sitename.com/httpdocs/tmp`.

Это мы и дописываем или исправляем в `configuration.php` в директории установки Jooomla.

```php
public $log_path = '/var/www/vhosts/sitename.com/httpdocs/logs';
public $tmp_path = '/var/www/vhosts/sitename.com/httpdocs/tmp';
public $upload_tmp_dir = '/var/www/vhosts/sitename.com/httpdocs/tmp';
```

Теперь загрузка изображений должна работать. Разумеется если проблема была в пути к директориям.
Дополнительно можно проверить права доступа к вышеуказанным директориям. 