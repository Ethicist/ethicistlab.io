---
layout: post
author: Ethicist
title: Установка Budgie Desktop одной командой
description: >
  Установка Android SDK из терминала в несколько строк. 
categories: blog
tags: >
  budgie sdk bash script desktop environment de linux
image:
  path: /budgie/Budgie.jpg
  caption: "Solus Budgie. A feature-rich, luxurious desktop using the most modern technologies."
  alt: Budgie
---

{% include media-post-image.html %}

## Установка

```bash

# WARNING! Running this command may HARM your system or shit your DE. Be careful!
# ВНИМАНИЕ! Выполнение данной команды может навредить вашей системе или подпортить окружение рабочего стола!

sudo apt install aisleriot apturl apturl-common bamfdaemon budgie-desktop-common budgie-desktop-environment budgie-indicator-applet budgie-lightdm-theme budgie-wallpapers budgie-wallpapers-xenial budgie-welcome cups cups-filters dleyna-renderer eog espeak-data evolution-data-server-online-accounts gir1.2-clutter-gst-3.0 gir1.2-evince-3.0 gir1.2-rb-3.0 gir1.2-secret-1 gir1.2-totem-1.0 gir1.2-totem-plparser-1.0 gnome-calendar gnome-disk-utility gnome-mahjongg gnome-mines gnome-online-miners gnome-orca gnome-photos gnome-screenshot gnome-sudoku gnome-sushi grilo-plugins-0.2-base grilo-plugins-0.2-extra gstreamer0.10-gnomevfs gthumb gthumb-data guile-2.0-libs hplip libaccounts-glib0 libaccounts-qt5-1 libatk-adaptor libbamf3-2 libdmapsharing-3.0-2 libdw1 libecal-1.2-19 libedataserverui-1.2-1 libespeak1 libexempi3 libfreehand-0.1-1 libgail-3-0 libgail-common libgfbgraph-0.2-0 libgjs0e libgom-1.0-0 libgom-1.0-common libgpod-common libgpod4 libgsf-1-114 libgsf-1-common liblua5.3-0 libmediaart-2.0-0 libmozjs-24-0v5 libmspub-0.1-1 libmusicbrainz5-2 libmusicbrainz5cc2v5 libpagemaker-0.0-0 libplank-common libplank1 libreoffice-avmedia-backend-gstreamer libreoffice-draw libreoffice-gnome libreoffice-impress libreoffice-pdfimport librhythmbox-core9 libsgutils2-2 libsignon-extension1 libsignon-glib1 libsignon-plugins-common1 libsignon-qt5-1 libtotem0 libtracker-control-1.0-0 libtracker-miner-1.0-0 libtracker-sparql-1.0-0 libunwind8 libxdo3 libzapojit-0.0-0 libzeitgeist-2.0-0 linux-hwe-tools-4.13.0-32 linux-tools-4.13.0-32-generic linux-tools-common linux-tools-virtual-hwe-16.04-edge media-player-info nautilus nautilus-share plank plymouth-theme-budgie-remix-logo plymouth-theme-budgie-remix-text plymouth-themes pocillo-icon-theme ppa-purge printer-driver-gutenprint printer-driver-hpcups printer-driver-postscript-hp python3-brlapi python3-louis python3-mako python3-markupsafe python3-notify2 python3-pyatspi python3-speechd rhythmbox rhythmbox-data rhythmbox-plugin-alternative-toolbar rhythmbox-plugin-tray-icon rhythmbox-plugin-zeitgeist rhythmbox-plugins s-nail sane-utils session-migration signon-plugin-password signon-ui signon-ui-service signon-ui-x11 signond simple-scan smartmontools speech-dispatcher speech-dispatcher-audio-plugins tlp tlp-rdw totem totem-common totem-plugins tracker tracker-extract tracker-miner-fs transmission transmission-gtk vertex-theme xdotool zeitgeist-core apg arc-theme budgie-core budgie-desktop cracklib-runtime cups-pk-helper dconf-cli dleyna-server faba-icon-theme gir1.2-budgie-desktop-1.0 gir1.2-ibus-1.0 gkbd-capplet gnome-bluetooth gnome-control-center gnome-control-center-data gnome-online-accounts gnome-screensaver gnome-session-bin gnome-session-common gnome-settings-daemon gnome-settings-daemon-schemas gnome-software gnome-software-common gnome-system-monitor gnome-terminal gnome-terminal-data gnome-user-share gstreamer1.0-clutter-3.0 ibus iio-sensor-proxy libbudgie-plugin0 libbudgietheme0 libcheese-gtk25 libcrack2 libdleyna-connector-dbus-1.0-1 libdleyna-core-1.0-3 libgeocode-glib0 libgnome-bluetooth13 libgnomekbd-common libgnomekbd8 libgoa-backend-1.0-1 libgrilo-0.2-1 libgupnp-av-1.0-2 libgupnp-dlna-2.0-3 libgweather-3-6 libgweather-common libibus-1.0-5 libmutter0g libpwquality-common libpwquality1 libraven0 libsnapd-glib1 libtelepathy-glib0 moka-icon-theme mousetweaks mutter-common nautilus-data pulseaudio-module-bluetooth realmd snapd-login-service system-config-printer-common system-config-printer-gnome system-config-printer-udev ubuntu-system-service unity-control-center-faces
```

Solus: [https://getsol.us/solus/about/](https://getsol.us/solus/) <br>
GitHub: [https://github.com/solus-project/budgie-desktop](https://github.com/solus-project/budgie-desktop)
