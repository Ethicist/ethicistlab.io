---
layout: post
author: Ethicist
title: "Создание nandroid backup устройства в adb shell"
description: >
  Создание nandroid-backup устройства на примере китайского планшета MRM-POWER. 
categories: blog
tags: >
  nandroid backup android adb shell tutorial linux bash
---

Прежде всего, на устройстве должен быть включен режим отладки и установлены root-права.
Подключаемся через USB к компьютеру.

Запускаем adb shell в режиме root, отображаем список смонтированных файловых систем.

```bash
$ adb shell
$ su
root@android:/ # cat /proc/mounts    
```

Наблюдаем следующее:

``` 
rootfs / rootfs rw 0 0
tmpfs /dev tmpfs rw,nosuid,relatime,mode=755 0 0
devpts /dev/pts devpts rw,relatime,mode=600,ptmxmode=000 0 0
proc /proc proc rw,relatime 0 0
sysfs /sys sysfs rw,relatime 0 0
none /acct cgroup rw,relatime,cpuacct 0 0

tmpfs /mnt/asec tmpfs rw,relatime,mode=755,gid=1000 0 0
tmpfs /mnt/obb tmpfs rw,relatime,mode=755,gid=1000 0 0

/dev/block/nandd /system ext4 rw,nodev,noatime,user_xattr,barrier=0,data=ordered 0 0
/dev/block/nande /data ext4 rw,nosuid,nodev,noatime,user_xattr,barrier=0,journal_checksum,data=ordered,noauto_da_alloc 0 0
/dev/block/nandh /cache ext4 rw,nosuid,nodev,noatime,user_xattr,barrier=0,journal_checksum,data=ordered,noauto_da_alloc 0 0

/dev/block/vold/93:72 /mnt/sdcard vfat rw,dirsync,nosuid,nodev,noexec,relatime,uid=1000,gid=1015,fmask=0702,dmask=0702,allow_utime=0020,codepage=cp437,iocharset=iso8859-1,shortname=mixed,utf8,errors=remount-ro 0 0
/dev/block/vold/93:72 /mnt/secure/asec vfat rw,dirsync,nosuid,nodev,noexec,relatime,uid=1000,gid=1015,fmask=0702,dmask=0702,allow_utime=0020,codepage=cp437,iocharset=iso8859-1,shortname=mixed,utf8,errors=remount-ro 0 0
tmpfs /mnt/sdcard/.android_secure tmpfs ro,relatime,size=0k,mode=000 0 0
```

Чтобы понять, что нам понадобится, выводим список разделов:

```bash
root@android:/ # cat /proc/partitions
```

major | minor | #blocks | name
 ---- | ----- | ------- | ----- 
  93  |    0  |   26384 | nanda
  93  |    8  |   16384 | nandb
  93  |   16  |   32768 | nandc
  93  |   24  |  524288 | nandd
  93  |   32  | 1228800 | nande
  93  |   40  |   16384 | nandf
  93  |   48  |   32768 | nandg
  93  |   56  |  262144 | nandh
  93  |   64  |  262144 | nandi
  93  |   72  | 5052656 | nandj
 179  |    0  | 7769088 | mmcblk0
 179  |    1  | 7768064 | mmcblk0p1
 
 Информация по разделам:
 
 /dev/block/nanda | bootloader (vfat)     | загрузочный раздел (U-Boot, script.bin, ...)
 /dev/block/nandb | env (u-boot-env)      | параметры для U-Boot
 /dev/block/nandc | boot (ANDROID)        | kernel+initrd
 /dev/block/nandd | system (ext4)         | /system
 /dev/block/nande | data (ext4)           | /data
 /dev/block/nandf | misc (special)        | (???) сервисный раздел bootloader для перехода в recovery
 /dev/block/nandg | recovery (ANDROID)    | (???) kernel+initrd режима RECOVERY
 /dev/block/nandh | cache (ext4)          | /cache
 /dev/block/nandi | private (vfat)        | /mnt/private
 /dev/block/nandj | sysrecovery (ANDROID) | (???) kernel+initrd режима SYSRECOVERY
 /dev/block/nandk | UDISK (vfat)          | /mnt/sdcard — раздел используемый в ANDROID как внутренняя карта памяти

Если раздел внешней карты памяти доступен только для чтения, монтируем карту памяти для чтения-записи:

```bash
$ mount -o remount,rw /mnt/extsd /mnt/extsd
```

Делаем резервные копии основных разделов /system /data и /cache. Пишем бэкап на внешнюю карту памяти устройства.

```bash
$ dd if=/dev/block/nandd of=/mnt/sdcard/nandd-system.img
$ dd if=/dev/block/nande of=/mnt/sdcard/nande-data.img
$ dd if=/dev/block/nandh of=/mnt/sdcard/nandh-cache.img
```

Если делаем полный бэкап, то пишем всё остальное:

```bash
$ dd if=/dev/block/nanda of=/mnt/extsd/nanda.img
$ dd if=/dev/block/nandb of=/mnt/extsd/nandb.img
$ dd if=/dev/block/nandc of=/mnt/extsd/nandc.img

# nandd и nande уже есть, пропускаем
$ dd if=/dev/block/nandf of=/mnt/extsd/nandf.img
$ dd if=/dev/block/nandg of=/mnt/extsd/nandg.img

# nandh уже есть, пропускаем
$ dd if=/dev/block/nandi of=/mnt/extsd/nandi.img
$ dd if=/dev/block/nandj of=/mnt/extsd/nandj.img
```

Для дальнейших манипуляций с копией раздела, монтируем образ:

```bash
$ mkdir test
$ sudo mount -t ext4 -o loop nandd.img test/
```

Процесс записи скопированного ранее раздела на устройство:

```bash
$ dd if=/mnt/sdcard/nandd-system.img of=/dev/block/nandd
$ dd if=/mnt/sdcard/nande-data.img of=/dev/block/nande
$ dd if=/mnt/sdcard/nandh-cache.img of=/dev/block/nandh
```