---
layout: post
author: Ethicist
title: Overriding mod_menu in Joomla 3
description: >
  In this tutorial you'll see how to simply override the default layout in site menu module and sort all links to separated arrays.
categories: blog
tags: >
  joomla tutorial php bootstrap 
image:
  path: /joomla/mod_menu.jpg
  caption: ""
  alt: Overriding mod_menu in Joomla 3
---

{% include media-post-image.html %}

In this tutorial you'll see how to simply override the default layout in site menu module and sort all links to separated arrays.

<span>1.</span> Go to Extensions > Templates > Templates > Your Template Details and Files;<br/>
<span>2.</span> Follow to next tab "Create overrides";<br/>
<span>3.</span> At "Modules" list choose "mod_menu";<br/>

If everything's fine, you'll see "Message Override created in /templates/yourtemplate/html/mod_menu Successfully created the override."
Otherwise you can get an error possible occurred with writing file. Check your template directory for read-write permissions, if they're not, set recursively "chmod 777" to your templates directory.

<span>4.</span> In your code editor / IDE, navigate to your templates directory.

Of course, you can simply override all "default_*.php" layouts. In this case I've just created two layouts for header and footer menu.
So, let's start coding!

File: /templates/template_name/html/mod_menu/<b>header-menu.php</b>

```php
<?php
foreach ($list as $i => &$item) {
    // gathering urls
    if ($item->deeper) {
        // dropdown title linkz
        $parent_url[] = $item->flink;
        $parent_names[] = $item->title;
        $parent_route[] = $item->route;
    } elseif ($item->level == 2) {
        // all dropdown dropdown
        $sublink_url[] = $item->flink;
        $sublink_names[] = $item->title;
        $sublink_route[] = $item->route;
    } elseif ($item->level == 1) {
        // other non-doropdown links
        $links_url[] = $item->flink;
        $links_names[] = $item->title;
        $links_route[] = $item->route;
    }
}

// sorting the links of the dropdown lists by arrays
// named as the routes of main links
foreach ($sublink_url as $sub_url) {
    // link position to match with name
    $index = array_search($sub_url, $sublink_url);
    foreach ($parent_route as $parent_node) {
        if (strpos($sub_url, $parent_node) !== false) {
            $dropdown_menu[$parent_node]['url'][] = $sub_url;
            $dropdown_menu[$parent_node]['title'][] = $sublink_names[$index];
        }
    }
}
?>

<!-- navbar block -->
<div class="fixed-top menu">
    <nav class="navbar navbar-expand-lg navbar-light top_nav">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbar" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span> Меню</button>
        <div class="collapse navbar-collapse justify-content-center" id="navbar">
            <ul class="navbar-nav">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Главная</a>
                    <?php
                        // Dropdown
                        echo '<div class="dropdown-menu">';
                        echo '<a class="dropdown-item" href="' . $parent_url[0] . '">Главная страница</a>';
                        for ($z = 0; $z <= count($dropdown_menu['home']['url']) - 1; $z++) {
                            echo '<a class="dropdown-item" href="' . $dropdown_menu['home']['url'][$z] . '">' . $dropdown_menu['home']['title'][$z] . '</a>';
                        }
                        echo '</div>';
                    ?>
                </li>

                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Потребителям</a>
                    <?php
                        // Dropdown
                        echo '<div class="dropdown-menu">';
                        for ($z = 0; $z <= count($dropdown_menu['consumers']['url']) - 1; $z++) {
                            echo '<a class="dropdown-item" href="' . $dropdown_menu['consumers']['url'][$z] . '">' . $dropdown_menu['consumers']['title'][$z] . '</a>';
                        }
                        echo '</div>';
                    ?>
                </li>
            </ul>
        </div>
    </nav>
</div>
<!-- /navbar block -->
```

And another one for footer menu:

File: /templates/template_name/html/mod_menu/<b>footer-menu.php</b>

```php
<?php
    foreach ($list as $i => &$item) {
        if ($item->deeper){
            $links_url[] = $item->flink;
            $links_names[] = $item->title;
        } elseif ($item->level == 2) {
            // we don't need dropdowns here
        } elseif ($item->level == 1) {
            $links_url[] = $item->flink;
            $links_names[] = $item->title;
        }
    }
    
    $output = '<div class="footer-menu">';
    $output .= '<ul class="list-group list-group-flush">';
    for ($i = 0; $i <= count($links_names)-1; $i++) {
        $output .= '<a href="' . $links_url[$i] . '"><li class="list-group-item">' . $links_names[$i] . '</li></a>';
    }
    $output .= '</ul>';
    $output .= '</div>';
	
    echo $output;
?>
```

<span>5.</span> If you already have complete the creating of menu items, you will need to create module for each menu layout.

For example: Footer menu, position - "footer-menu", type "menu"; Show Sub-menu Items is off cause in this example our footer menu has not dropdown links.

<span>6.</span> Go to "Advanced" tab and set our custom layout in the "Layout" option list called "---From YourTemplate--- / footer-menu".

Everything is done!