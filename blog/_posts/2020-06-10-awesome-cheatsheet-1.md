---
layout: post
author: Ethicist
title: Поваренная книга программиста 1
description: >
  Заметки об особенностях использования команд в linux
categories: blog
tags: >
  linux ffmpeg bash timidity aplaymidi sed badblocks tutorial
---

# ffmpeg

_Источник: [19 команд ffmpeg для любых нужд](https://habr.com/ru/post/171213/#comment_5940077)_

## Соединяем аудио и видео

```bash
$ ffmpeg -i input.mp3 -i input.mp4 -acodec copy -vcodec copy output.mp4
```

## Вырезаем кадр из видео

_Источник: [https://habr.com/post/171213/#comment_6263189](https://habr.com/post/171213/#comment_6263189)_

```bash
$ ffmpeg -ss 00:00:02 -i file.flv -f image2 -vframes 1  file_out.jpg
```

## Конвертируем видео в mp3

```bash
$ ffmpeg -i ./in.mp4 -codec:a libmp3lame -qscale:a 0 ./output.mp3
```
или

```bash
$ ffmpeg -i ./in.mp4 -codec:a libmp3lame -b:a 320k ./output.mp3
```

|    -i ./in.mp4            | путь к файлу     |
|    -codec:a libmp3lame    | кодек libmp3lame |
|    -qscale:a 0            | VBR 0            |
|    -b:a 320k              | CBR 320kbps      |
|    ./output.mp3           | вывод файла      |
    
## Вырезаем фрагмент из видео

```bash
$ ffmpeg -i ./in.mp4 -acodec copy -vcodec copy -ss 00:00:00 -t 00:02:45 ./out.mp4
```

|    -i ./in.mp4        | путь к файлу              |
|    -acodec copy       | копируем аудио кодек      |
|    -vcodec copy       | копируем видео кодек      |
|    -ss 00:00:00       | начало обрезка в hh:mm:ss |
|    -t  00:02:45       | длина обрезка в hh:mm:ss  |
|    ./out.mp4          | вывод нового файла        |
    
## Повернуть видео на 90°

_Источник: [https://habr.com/post/171213/#comment_5940077](https://habr.com/post/171213/#comment_5940077)_

```bash
$ ffmpeg -vf transpose=1 -i file.avi file1.avi
```

Значение transpose:
  0 — против часовой стрелки и зеркально;
  1 — по часовой стрелке;
  2 — против часовой стрелки;
  3 — по часовой стрелке и зеркально
 
## Конвертирование

```bash
# flac > alac
$ for f in ./*.flac; do ffmpeg -i "$f" -c:a alac "${f%.*}.m4a"; done
   
# wav > mp3
$ for f in ./*.wav; do ffmpeg -i "$f" -codec:a libmp3lame -qscale:a 0 "${f%.*}.mp3"; done
   
# flac > mp3
$ for f in ./*.flac; do ffmpeg -i "$f" -codec:a libmp3lame -qscale:a 0 "${f%.*}.mp3"; done
```

## Конвертируем MIDI в MP3 _(timidity + ffmpeg)_

```bash
$ timidity input.mid -Ow -o - | ffmpeg -i - -acodec libmp3lame -b:a 128k output.mp3
```

# Форматирование текста

## Удалить "CR" из файла

```bash
$ sed -i -e 's/\r$//' ./file.txt
```

## Убрать пустые строки в файле

```bash
$ grep -v '^$' input.txt > output.txt
```

## Удаление пробелов и табуляций

```bash
$ sed -i 's/^[ \t]*//;s/[ \t]*$//' file_name.txt
```

# Прочее

## Форматирование в FAT32

```bash
$ mkfs.vfat -F32 -c sdXX
```

## Проверка носителя на бэды с последующей пометкой _(badblocks + e2fsck )_

```bash
$ badblocks -svw /dev/sdXX > /path/to/bad.list
$ e2fsck -l /path/to/bad.list /dev/sdXX
```

|    -svw      |  проходим тремя алгоритмами |


## OptiPNG - Максимальное сжатие png без записи IDAT, самый медленный метод 

```bash
$ optipng -zc1-9 -zm1-9 -zs0-3 -f0-5 -nz file.png
```

|    -zc1-9       |   zlib compression levels (от 1 до 9)     |
|    -zm1-9       |   zlib memory levels (от 1 до 9)          |
|    -zs0-3       |   zlib compression strategies (от 0 до 3) |
|    -f0-5        |   PNG delta filters (от 0 до 5)           |
|    -nz          |   no IDAT recoding                        |
	
То же самое, только короткой записью:
	
```bash 
$ optipng -o7 -nz file.png
```
    
#№ Вопроизвести MIDI

```bash 
$ aplaymidi --port 128:0 input.mid
```
    
#№ Найти все файлы "firefox" и вывести их пути в кавычках

```bash 
$ locate firefox | sed 's/.*/"&"/' 
```

## Сложное: команда с pipe, регулярками и xargs

1. Найти всю музыку в "/music/"
2. Вывести полные пути к файлам
3. Выбрать все пути к файлам с расширением ".m4a"
4. Запаковать всё это в tar

```bash 
$ ls -R "/music/" | awk '/:$/&&f{s=$0;f=0}/:$/&&!f{sub(/:$/,"");s=$0;f=1;next}NF&&f{print s$0}' | grep ".m4a" | sed 's/.*/"&"/' | xargs tar cvf music.tar
```
