---
layout: post
author: Ethicist
title: Установка BusyBox в Windows и загрузка файлов из списка wget
description: >
  Установка и удаление команд BusyBox в Windows, а также загрузка файлов из списка wget
categories: blog
tags: >
  busybox windows install tutorial wget script
image:
  path: /2021/busybox-wgetlist/busybox_wgetlist.png
  caption: "Загрузка нескольких файлов списком в wget (busybox)"
  alt: busybox_wgetlist.png
---

{% include media-post-image.html %}

### Загружаем busybox:
*32 bit*: [https://frippery.org/files/busybox/busybox.exe](https://frippery.org/files/busybox/busybox.exe) <br/>
*64 bit*: https://frippery.org/files/busybox/busybox64.exe

После чего перемещаем `busybox.exe` в `%windir%/System32/`

### Установка отдельных команд busybox:

Запускаем командную строку с правами администратора.

```batch
cd %windir%/System32/
busybox --list>busybox.txt
for /F %i in (busybox.txt) do echo busybox %i ^%^*> %i.cmd
del busybox.cmd
del busybox.txt
```

### Удаление команд busybox из системы:

```batch
busybox --list>busybox.txt
for /F %i in (busybox.txt) do del %i.cmd
del busybox.txt
```

### BusyBox wget: загрузка файлов из списка

```batch
cd %windir%/System32/
echo for /F ^%^%i in (^%^*) do wget ^%^%i > wgetlist.cmd
```

### Пример использования:

```batch
cd path_to_store_downloads
```

Добавляем файл со списком ссылок `download.urls`

```
https://frippery.org/files/busybox/busybox.exe
https://frippery.org/files/busybox/busybox.exe.sig
https://frippery.org/files/busybox/busybox64.exe
https://frippery.org/files/busybox/busybox64.exe.sig
https://frippery.org/files/busybox/busybox_glob.exe
https://frippery.org/files/busybox/busybox_glob.exe.sig
```

```batch
wgetlist download.urls
```