---
layout: page
title: Обо мне
permalink: /about/
---

# 👋 Меня зовут Глеб
И в данный момент я нахожусь в поиске работы, где мог бы в полной мере проявить свои навыки и получить новый опыт.

# Что я умею?

## 🌐 WEB:
- Верстка: HTML, CSS, SASS, LESS
- Фреймворки: [Bootstrap](https://getbootstrap.com), [Material Design Light](https://getmdl.io)
- Генератор статических сайтов [Jekyll](https://jekyllrb.com)
- Публикация статики: [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/), [Surge](http://surge.sh)
- Скриптинг: JavaScript, jQuery <br>
- _Презентации_: [impress.js](https://github.com/impress/impress.js) _(Да, они умеют в 3D)._

К слову, [список моих завершённых проектов](../projects/).

## 📱 Мобильные платформы
- Разработка приложений на WebView под Andoid <br>
_(это когда сайт скачивается и открывается как нативное приложение)_
- Разработка Desktop-приложений c [NeutralinoJS](https://github.com/neutralinojs/neutralinojs) <br>
_(это как [ElectronJS](https://www.electronjs.org), только без Chromium и размером 5 мегабайт)_
- Разработка игр и приложений на [Java 2 Micro Edition](https://ru.wikipedia.org/wiki/Java_Platform,_Micro_Edition) <br>
_(даже несмотря на то, что платформа морально устарела, я ценю её, как сейчас многие ценят Dendy или Sega)_

## 🛠️ Софт, инструменты
- Редакторы: [Notepad++](https://notepad-plus-plus.org), [GEdit](https://ru.wikipedia.org/wiki/Gedit), [Brackets](http://brackets.io), [Typora](https://typora.io)
- Среда разработки: [NetBeans IDE](https://netbeans.org), Nokia IDE for Java ME (Eclipse)
- Графика: [Inkscape](https://inkscape.org/ru/), [GIMP](https://www.gimp.org)
- Макеты: [Figma](https://www.figma.com)
- ОС: [Debian](https://www.debian.org), [Ubuntu](https://ubuntu.com), [CrunchBangPlusPlus](https://crunchbangplusplus.org)
- Офис: [WPS Office](https://www.wps.com), [LibreOffice](https://www.libreoffice.org), [Google Docs](https://docs.google.com/)
- САПР/CAD: [КОМПАС-3D](https://kompas.ru), [LibreCAD](https://librecad.org)
- Репозитории: [GitLab](https://gitlab.com/Ethicist)

## ⚖️ Лицензии
За всё время работы с самым различным кодом немного поднатаскался на свободных лицензиях. Могу подобрать необходимый вариант для различных проектов. В работе предпочитаю пользоваться свободным ПО и максимально свободным лицензиями.

# Как со мной связаться?
Электропочта:
- [cryingdaemon@gmail.com](mailto:cryingdaemon@gmail.com)
- [ethicist@post.cz](mailto:ethicist@post.cz)

Чаще, чем на почте, я бываю [ВКонтакте](https://vk.com/ethicist) и в [Telegram](https://t.me/dvluper).

