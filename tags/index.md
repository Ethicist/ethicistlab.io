---
layout: page
title: Теги
permalink: /tags/
---

{% if site.posts.size == 0 %}
  <h2>Ничего не найдено</h2>
{% endif %}

<input type="text" id="quicksearch" placeholder="Быстрый поиск" />
<div class="tags">
  <ul class="label" id="tags-container"></ul>
</div>

<script>
  let list = [
    {% for tag in site.tags %}
      '{{ tag[0] }}',
    {% endfor %}
  ];
  
  const result = document.getElementById('tags-container');
  
  render_list(list, result);
  
  function filter(val, list) {
    let result;
    list.forEach (
      item => {
        if(item.indexOf(val) != -1) {
          result.push(item);
        }
      }
    );
    return result;
  }
  
  function filter(val, list) {
    return list.filter(
      item => (
        item.includes(val)
      )
    );
  }
  
  function render_list(param_list = [], el = document.body) {
	  el.innerHTML = '';
    param_list.forEach(
      data => {
        let list_item = document.createElement('li');
        let list_item_link = document.createElement('a');
        let list_item_text = document.createElement('span');
        list_item_text.innerHTML = data;
        list_item_link.href = '/tags/' + data;
        list_item_link.appendChild(list_item_text);
        list_item.appendChild(list_item_link);
        el.appendChild(list_item);
      }
    )
  }
  
  document.getElementById('quicksearch').addEventListener(
    'input',
    event => {
      render_list(filter(event.target.value, list), result);
    }
  );
</script>

<br />
