---
layout: post
author: Ethicist
title: Фоновое изображение на всю страницу 
description: >
  Последнее время довольно востребованный элемент.
categories: pastebin
tags: >
  html css background design codepen
---

## Первый вариант

```css
html {
  margin: 0;
  padding: 0;
  background-color: #493544 !important;
  background-image: url(./background.jpg);
  background-position: center center;
  background-repeat: no-repeat;
  background-attachment: fixed;
  -webkit-background-size: cover;
  -moz-background-size: cover;
  -o-background-size: cover;
  background-size: cover;
  width: 100%;
  height: 100%;
}
```

## Второй вариант, ещё интересней

```html
<div class="background"></div>
```

```css
/* * * * * * * * * * * * * * * * * * * * * * * *
 * Full-page background image
 *
 * Using "Clear Text Over Any Background"
 * https://codepen.io/chris22smith/pen/jRGPrN
 */
 
@keyframes onload {
	0% {
		opacity: 0;
	}
	100% {
		opacity: 1;
	}
}

.background {
    background-color: transparent;
    background-image: url(./background.jpg);
    background-repeat: no-repeat !important;
    background-position: center !important;
    background-attachment: fixed !important;
    -webkit-background-size: cover !important;
    -moz-background-size: cover !important;
    -o-background-size: cover !important;
    background-size: cover !important;
    filter: contrast(.6) brightness(.6);
    position: fixed;
    bottom: 0;
    right: 0;
    left: 0;
    top: 0;
    z-index: -17;
    animation: 1s ease-in-out 0s 1 onload;
    -webkit-transition: 1s;
    transition: 1s;
}
```

<p class="codepen" data-height="265" data-theme-id="light" data-default-tab="css,result" data-user="ethicist" data-slug-hash="wvMBbYE" style="height: 265px; box-sizing: border-box; display: flex; align-items: center; justify-content: center; border: 2px solid; margin: 1em 0; padding: 1em;" data-pen-title="Full-page background image">
  <span>See the Pen <a href="https://codepen.io/ethicist/pen/wvMBbYE">
  Full-page background image</a> by Captain Anonymous (<a href="https://codepen.io/ethicist">@ethicist</a>)
  on <a href="https://codepen.io">CodePen</a>.</span>
</p>
<script async src="https://static.codepen.io/assets/embed/ei.js"></script>


Источник: [CSS-tricks](https://css-tricks.com/perfect-full-page-background-image/)