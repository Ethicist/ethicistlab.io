---
layout: post
author: Ethicist
title: "Git Command line instructions"
description: >
  Git Command line instructions for GitLab
categories: pastebin
tags: >
  git gitlab bash
---

## Git global setup
```bash
git config --global user.name "username"
git config --global user.email "email"
```

## Create a new repository
```bash
git clone https://gitlab.com/username/repository.git
cd glare
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master
```

## Push an existing folder
```bash
cd existing_folder
git init
git remote add origin https://gitlab.com/username/repository.git
git add .
git commit -m "Initial commit"
git push -u origin master
```

## Push an existing Git repository
```bash
cd existing_repo
git remote rename origin old-origin
git remote add origin https://gitlab.com/username/repository.git
git push -u origin --all
git push -u origin --tags
```