---
layout: post
author: Ethicist
title: Случайный цвет фона на JavaScript
description: >
  Случайный цвет фона на JavaScript
categories: pastebin
tags: >
  javascript source
---

```javascript
function generateRandomBG() {
  var r, g, b, i, f, p, q, t, h, s, v;
  h = Math.random();
  s = 0.31;
  v = 0.68;
  i = Math.floor(h * 6);
  f = h * 6 - i;
  p = v * (1 - s);
  q = v * (1 - f * s);
  t = v * (1 - (1 - f) * s);
  switch (i % 6) {
    case 0: r = v, g = t, b = p; break;
    case 1: r = q, g = v, b = p; break;
    case 2: r = p, g = v, b = t; break;
    case 3: r = p, g = q, b = v; break;
    case 4: r = t, g = p, b = v; break;
    case 5: r = v, g = p, b = q; break;
  }
  return { r: Math.floor(r * 255), g: Math.floor(g * 255), b: Math.floor(b * 255) };
}

function changeBG() {
  var color = generateRandomBG();
  document.body.style.backgroundColor = "rgb(" + color.r + "," + color.g + "," + color.b + ")";
}
```