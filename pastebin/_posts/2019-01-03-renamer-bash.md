---
layout: post
author: Ethicist
title: Массовое переименование файлов из cp1251 в utf-8
description: >
  Program to convert file names from cp1251 to utf-8 encoding
categories: pastebin
tags: >
  bash rename
---

```bash
#! /bin/bash

usage="\n Program to convert file names from cp1251 to utf-8 encoding\n
  ...or just another fucking useless untitled batch file renamer\n\n
  Arguments:\n
  \t-d, --dir \tdirectory containing files to rename\n
  \t-h, --help \tdisplay this help and exit\n
  \t-r, --reverse \tconvert names from utf-8 to cp1251\n
  \t-v, --version \toutput version information and exit\n
  \n
  Examples:\n
  \tFind all files with cp1251 characters and move to new directory:\n
  \t$(basename "$0") -d '/media/Music/Бутырка/'\n
  \n
  \tMultiple renaming:\n
  \t$(basename "$0") '/media/Files/Ãîâíî.xlsx' '/media/Files/Ññàíèíà.mp3' \n"

version="\n Version 0.0.1 (January 03, 2019)\n
  Code by Ethicist: https://vk.com/ethicist \n
  \n        
  LICENSE\n
  \n
  This software is in the public domain. Where that dedication is not\n
  recognized, you are granted a perpetual, irrevocable license to copy,\n
  distribute, and modify this file as you see fit.\n"

charset_utf8='АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯабвгдеёжзийклмнопрстуфхцчшщъыьэюя'
charset_cp1251='ÀÁÂÃÄÅšÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖ×ØÙÚÛÜÝÞßàáâãäåžæçèéêëìíîïðñòóôõö÷øùúûüýþÿ'

param_input_dir=0
param_reversed=0
param_multiple=0

if [ $# -lt 1 ]
then
  echo -e $usage
  exit 0
fi

for args in "$@"
do
  case $args in
    -d*|--dir*)
      let "param_input_dir += 1"
       input_dir="$2"
      shift
    ;;
    -r|--reverse)
      let "param_reversed += 1"
      shift
    ;;
    -h|--help)
      echo -e $usage
      exit
    ;;
    -v|--version)
      echo -e $version
      exit
    ;;
    *)
      let "param_multiple += 1"
    ;;
  esac
done

if [ "$param_reversed" -eq 1 ]
then
  regexp="s/.*/&/;y|$charset_utf8|$charset_cp1251|"
else
  regexp="s/.*/&/;y|$charset_cp1251|$charset_utf8|"
fi

if [ "$param_input_dir" -eq 1 ]
then
  cd $input_dir
  for filename in *
  do
    echo Renaming $files
    mv "$filename" "`echo $filename | sed -e $regexp`"
  done
else
  for filename in "$@"
  do
    echo -e "\n Renaming $filename"
    mv "$filename" "`echo $filename | sed -e $regexp`"
  done
fi
echo

exit 0
```