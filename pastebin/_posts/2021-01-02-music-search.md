---
layout: post
author: Ethicist
title: "Ссылки для легального поиска музыки"
description: >
  Ссылки для легального поиска музыки в Spotify, Яндекс Музыке и т.д.
categories: pastebin
tags: >
  music url search query
---

Небольшой список ссылок для встраивания легального поиска музыки.<br/>
Здесь *REQUEST* заменяется на поисковый запрос в формате URL.

+ https://google.com/?gws_rd=ssl#newwindow=1&q=*REQUEST*
+ https://open.spotify.com/search/*REQUEST*
+ https://music.yandex.ru/search?text=*REQUEST*
+ https://vk.com/search?c[q]=*REQUEST*&c[section]=audio
+ https://youtube.com/results?search_query=*REQUEST*
+ https://deezer.com/search/*REQUEST*
+ https://soundcloud.com/search?q=*REQUEST*
+ https://last.fm/search?q=*REQUEST*
+ https://junodownload.com/search/?q[all][]=*REQUEST*
+ https://beatport.com/search?q=*REQUEST*
