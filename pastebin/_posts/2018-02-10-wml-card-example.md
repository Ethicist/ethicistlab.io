---
layout: post
author: Ethicist
title: WML cards example
description: >
  WML cards example
categories: pastebin
tags: >
  wml xml example
---

```xml
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE wml PUBLIC "-//WAPFORUM//DTD WML 1.1//EN" "http://www.wapforum.org/DTD/wml_1.1.xml">
<wml>

  <card id="index" title="WML card example" newcontext="true">
    <p align="left">Welcome to WAP site</p>
    <p align="left">Your browser is up-to-date?<br/>Possibly here you can see only source code, not page.<br/>Please use this <a href="https://chrome.google.com/webstore/detail/wml/obhgkibgbjdfojpkmoaanffdkcfpdodb">extension</a> or get the fuck away! TNX.</p>
    <p>
      <a href="#page1">What is this?</a><br/>
      <a href="#page2">Necrophilia?!</a><br/>
    </p>
  </card>

  <card id="page1" title="Chapter I: Introduction in WML" newcontext="true">
    <p>A long time ago, in a galaxy far, far away...</p>
    <p align="center">
      <a href="#index">Back</a><br/>
    </p>
  </card>

  <card id="page2" title="Dead technologies! Yuum. I like it." newcontext="true">
    <p>Here is some informatio about me. I can't write too much in here, though as there is not much space<br/>
       However you can scroll down the screen, of course.
    </p>
    <p align="center">
      <a href="#index">Back</a><br/>
    </p>
  </card>
  
</wml>
```