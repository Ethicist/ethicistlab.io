---
layout: post
author: Ethicist
title: Проверка методов генерации случайных чисел
description: >
  Проверка методов генерации случайных чисел
categories: pastebin
tags: >
  java j2me random midlet test
---

RandomMIDlet.java

```java
import java.util.Random;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Display;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Form;
import javax.microedition.lcdui.List;
import javax.microedition.lcdui.StringItem;
import javax.microedition.lcdui.TextField;
import javax.microedition.midlet.*;

/**
 * @author Ethicist
 */
public class RandomMIDlet extends MIDlet implements CommandListener  {
    
    private final Command generate;
    private final Command cmd_back;
    private final Command cmd_reset;
    private final Command cmd_exit;
    private final Command cmd_okay;
    private Display display;
    private final TextField tf1, tf2;
    private final List mainMenu;
    private Form first, second;
    private final Random random;
    
    public RandomMIDlet(){
      generate = new Command("Run", 1, 1);
      cmd_back = new Command("Back", 2, 1);
      cmd_reset = new Command("Reset", 3, 1);
      cmd_exit = new Command("Exit", 7, 1);
      cmd_okay = new Command("Select",1,1);
      mainMenu = new List("Random Test", 3);
      mainMenu.append("Simple mode", null);
      mainMenu.append("Advanced mode", null);
      mainMenu.append("Exit", null);
      mainMenu.setSelectCommand(cmd_okay);
      mainMenu.setCommandListener(this);
      random = new Random();
      tf1 = new TextField("Size:", "17", TextField.NUMERIC, 5);
      tf2 = new TextField("Size:", "17", TextField.NUMERIC, 5);
    }

    public void startApp() {
      display = Display.getDisplay(this);
      display.setCurrent(mainMenu);
    }
    
    public void pauseApp() { /* ... */ }
    
    public void destroyApp(boolean unconditional) { /* ... */ }
    
    private void showForm1() {
      first = new Form("Random Test: Simple mode");
      first.append(tf1);
      first.addCommand(generate);
      first.addCommand(cmd_back);
      first.addCommand(cmd_reset);
      first.setCommandListener(this);
      display.setCurrent(first);
    }
    
    private void showForm2() {
      second = new Form("Random Test: Advanced");
      second.append(tf2);
      second.addCommand(generate);
      second.addCommand(cmd_back);
      second.addCommand(cmd_reset);
      second.setCommandListener(this);
      display.setCurrent(second);
   }
    
    private int getSimpleNumber(int size) {
      int magic = Math.abs((random.nextInt() >>> 1) % size - 1);
      if (size == 0) {
        return 0;
      } else {
        return magic;
      }
    }
    
    private int getAdvancedNumber(int size) {
      int[] m = new int[size];
      for (int i = 0; i < size; i++) {
        m[i] = Math.abs((random.nextInt() >>> 1) % size - 1);
      }
      int magic = m[Math.abs(random.nextInt() >>> 1) % size];
      if (size == 0) {
        return 0;
      } else {
        return magic;
      }
    }

  public void commandAction(Command c, Displayable d) {
    if (c == cmd_exit) {
      destroyApp(false);
      notifyDestroyed();
    }
    if(c == cmd_okay) {
        int itemIndex = mainMenu.getSelectedIndex();
        switch (itemIndex) {
          case 0: {
            showForm1();
            break;
          }
          case 1: {
            showForm2();
            break;
          }
          case 2: {
            destroyApp(false);
            notifyDestroyed();
            break;
          }
        }
    }      
    if (d == first) {
      if (c == generate) {
        first.deleteAll();
        StringItem temp = new StringItem("Simple mode:", null);
        first.append(temp);
        int size = Integer.parseInt(tf1.getString());
        for (int i = 1; i <= 100; i++) {
          first.append(i+" = "+getSimpleNumber(size)+"\r"+"\n");
        }
      } else if (c == cmd_back) {
        display.setCurrent(mainMenu);
        first = null;
        second = null;
      } else if (c == cmd_reset) {
        first.deleteAll();
        tf1.setString("17");
        first.append(tf1);
      }
    } else if (d == second) {
      if (c == generate) {
        second.deleteAll();
        StringItem temp = new StringItem("Advanced mode", null);
        second.append(temp);
        int size = Integer.parseInt(tf2.getString());
        for (int i = 1; i <= 100; i++) {
          second.append(i+" = "+ getAdvancedNumber(size)+"\r"+"\n");
        }
      } else if (c == cmd_back) {
        display.setCurrent(mainMenu);
        first = null;
        second = null;
      } else if (c == cmd_reset) {
        second.deleteAll();
        tf2.setString("17");
        second.append(tf2);
      }
    } 
  }
}
```