---
layout: post
author: Ethicist
title: "Как установить общий доступ к /var/www ?"
description: >
  How can I chmod 777 all subfolders of /var/www?
categories: pastebin
tags: >
  chmod bash unix
---

Перевод не завершён.

Files and directories in Unix may have three types of permissions: read (r), write (w), and execute (x). 
Each permission may be on or off for each of three categories of users: the file or directory owner; 
other people in the same group as the owner; and all others. To change the mode of a file, use the chmod
command. The general form is chmod X@Y file1 file2 …

<blockquote>
Файлы и каталоги в Unix могут иметь три типа разрешений: чтение (r), запись (w) и выполнение (x).
Каждое разрешение может быть включено или выключено для каждой из трех категорий пользователей:
владелец файла или каталога; другие люди в той же группе, что и владелец; и все остальные.
Чтобы изменить режим работы файла, используйте команду chmod. Общая форма-chmod X@Y file1 file2 …
</blockquote>

```bash
chmod a-w file         # Removes all writing permissions
chmod o+x file         # Sets execute permissions for other (public permissions)
chmod u=rx file        # Give the owner rx permissions, not w
chmod go-rwx file      # Deny rwx permission for group, others
chmod g+w file         # Give write permission to the group
chmod a+x file1 file2  # Give execute permission to everybody
chmod g+rx,o+x file    # OK to combine like this with a comma
```

u = user that owns the file<br/>
g = group that owns the file<br/>
o = other (everyone else)<br/>
a = all (everybody)<br/>
<br/>
r = read aces to the file<br/>
w = write access<br/>
x = execute (run) access<br/>

Источник: [How can I chmod 777 all subfolders of /var/www? (askubuntu.com)](https://askubuntu.com/a/809922)