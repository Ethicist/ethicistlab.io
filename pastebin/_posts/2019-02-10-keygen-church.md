---
layout: post
author: Ethicist
title: Keygen Church
description: >
  Keygen Church
categories: pastebin
tags: >
  keygen church cmd artwork
image:
  path: /keygen-church/post.jpg
  caption: "░ ▒ ▓ █ by KEYGEN CHURCH"
  alt: ░ ▒ ▓ █ by KEYGEN CHURCH
---

<center>
<br>
{% include media-post-image.html %}
</center>

```batch
@echo off
@chcp 65001
color 0C
title = ░ ▒ ▓ █
@cls
@echo/
@echo/
@echo                                      ▓  ▓  ▓      
@echo                                      ▒  ▒  ▒      
@echo                                   ▓  ░░░▒░░░  ▓   
@echo                                   ▒     ▒     ▒   
@echo                                   ░░░░▒▒▒▒▒░░░░   
@echo                                         ▒         
@echo                                     ▓▒░░▒░░▒▓     
@echo                                        ▒ ▒        
@echo                                       ▒   ▒       
@echo                                       ▒   ▒       
@echo                                        ▒▒▒     
@echo/
@echo/
@echo                                      ░ ▒ ▓ █      
@echo/
@echo/
@pause
@exit /b
```

{%
	include media-iframe.html
	url="https://bandcamp.com/EmbeddedPlayer/album=1355848298/size=large/bgcol=ffffff/linkcol=de270f/tracklist=false/artwork=small/transparent=true/"
	caption=""
%}