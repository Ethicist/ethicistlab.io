---
layout: post
author: Ethicist
title: Загрузчик MIDI-файлов
description: >
  Автоматизация загрузки большого количества midi-файлов.
categories: pastebin
tags: >
  midi bash script random
---

Автоматизация загрузки большого количества midi-файлов.
Используются генераторы: [anticulture.net](http://anticulture.net/MusicGenerators.php)

```bash
#!/bin/bash

# Code by Ethicist <ethicist@volny.cz>
# Sat April 28 19:38:00 +06 2018
# All used MIDI generarots provided by anticulture.net
# are under the copyright of their owners. <Neotenic>

checkdir() {
  if [ ! -f $1 ]; then
    mkdir -p -m 777 $1
  fi
}

set_dir() {
  checkdir "$BASE/$1"
  DL_DIR="$BASE/$1"
  cd $DL_DIR
}

open() {
  if hash xdg-open 2>/dev/null
    then xdg-open $1
  elif hash gnome-open 2>/dev/null
    then gnome-open $1
  else
    echo; echo "  Cannot detect @ open()"; echo;
    sleep 3
    title_screen
  fi
}

checkdir "$HOME/.local/share/anticulture/" 
BASE="$HOME/.local/share/anticulture"
DL_DIR="$HOME/.local/share/anticulture/"

title_screen() {
  clear
  echo "  ____________________________________________________________________________"
  echo
  echo "   Batch MIDI Downloader                                      ANTICULTURE.NET "
  echo "  ____________________________________________________________________________"
  echo
  echo "    1. Jazz Fusion Generator"
  echo "    2. Hip Hop Beat Generator"
  echo "    3. Random Modern Jazz"
  echo "    4. Random Trance Music"
  echo
  echo "    5. Exit"
  echo "  ___________________________________________________________ Code by Ethicit "
  echo
  read -n 1 -p "  > " generator
  echo
  case $generator in
    1) set_dir "JazzFusionGenerator" ;
       URL="http://www.anticulture.net/JazzFusionGenerator" ;;
    2) set_dir "HipHopBeatGenerator" ;
       URL="http://www.anticulture.net/HipHopBeatGenerator" ;;
    3) set_dir "JazzGenerator" ;
       URL="http://www.anticulture.net/JazzGenerator.php" ;;
    4) set_dir "RandomTranceMusic" ;
       URL="http://www.anticulture.net/RandomTranceMusic.php" ;;
    5) exit 0 ;;
    *) echo; echo "  Wrong answer \"$generator\"."; sleep 2; echo; title_screen ;;
  esac
  echo
  read -p "  Amount of files: " amount
  [ -z "${amount//[0-9]}" ] && [ -n "$amount" ] || echo "  Wrong parameter $amount."
}

start_download() {
  for (( i=1; i <= $amount; i++ )) do
    echo
    echo "  Downloading: $i / $amount  ⋅ ⋅ ⋅ ⋅ ⋅ ⋅ ⋅ ⋅ ⋅ ⋅ ⋅ ⋅ ⋅ ⋅ ⋅ ⋅ ⋅ ⋅ ⋅ ⋅ ⋅ ⋅ ⋅ ⋅  $i.mid"
    echo
 #  echo "    i = $i" ; echo "    DL_DIR = $DL_DIR" ; echo "    URL = $URL" ; echo ;
    curl -# -o "$DL_DIR/$i.mid" "$URL"
  done
}

ask_to_open() {
  echo
  read -r -p "  Download finished. Open destination folder? (Y/N): " open_answer
  case $open_answer in
    ['y''Y']['e''E']['s''S']|['y''Y']) open $DL_DIR; exit 0 ;;
    ['n''N']['o''O']|['n''N']) ;; # DO NOTHING 
    *) echo; echo "  ! Wrong answer $open_answer."; sleep 2; echo; exit 0 ;;
  esac
}

press_any_key() {
  echo
  read -n 1 -r -s -p "  Press any key to continue..." key
  echo
}

title_screen
start_download
ask_to_open
press_any_key
exit 0
```
