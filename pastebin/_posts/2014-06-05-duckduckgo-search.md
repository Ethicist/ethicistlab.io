---
layout: post
author: Ethicist
title: SearchEngine.java
description: >
  SearchEngine.java
categories: pastebin
tags: >
  java j2me mobile midlet search duckduckgo
---

```java
/* 
    Document    : SearchEngine.java
    Version     : 0.0.25 (beta)
    Created on  : 05.06.2014, 13:37
    Author      : The Running Man
    Description : DDG Search client
*/

import java.io.InputStream;
import javax.microedition.io.ConnectionNotFoundException;
import javax.microedition.lcdui.*;
import javax.microedition.midlet.MIDlet;
import org.netbeans.microedition.lcdui.SplashScreen;

public class SearchEngine extends MIDlet
    implements CommandListener, ItemCommandListener {

    public Command gotoCommand;
    public Form searchForm, helpForm;
    public SplashScreen splashScreen;
    public Command exitCommand;
    public Command srchCommand;
    public Command helpCommand;
    public Command backCommand;
    public Command wikiCommand;
    public Command clearCommand;
    public StringItem srchButton;
    public StringItem info, link;
    public StringItem wikiLink;
    public TextField textField;
    public ImageItem header;
    public Display mainDisplay;
    public Image headerImage;
    public Alert defAlert;
    public String url1, url2, url3;
    
    public SearchEngine() {
        mainDisplay = Display.getDisplay(this);
        gotoCommand = new Command(" OK ", Command.OK, 0);
        exitCommand = new Command("Exit", Command.EXIT, 1);
        backCommand = new Command("Back", Command.BACK, 0);
        helpCommand = new Command("Information", Command.HELP, 0);
        srchCommand = new Command("Search", Command.OK, 0);
        wikiCommand = new Command(" OK ", Command.OK, 0);
        clearCommand = new Command("Clear field", Command.SCREEN, 0);
        info = new StringItem("DuckDuckGo\n", getText("/info.txt"));
        info.setFont(Font.getFont(0,0,8));
        headerImage = getImage("/header.png");
        header = new ImageItem(null, headerImage, 3, null);
        textField = new TextField(null, "", 1000, 4);
        srchButton = new StringItem(null, "Search", 2);
        srchButton.setLayout(3|0x4000);
        srchButton.setDefaultCommand(srchCommand);
        srchButton.setItemCommandListener(this);
        url1 = "https://duckduckgo.com/";
        link = new StringItem("Official website\n", url1, 1);
        link.setFont(Font.getFont(0,0,8));
        link.addCommand(gotoCommand);
        link.setItemCommandListener(this);
        url2 = "http://en.wikipedia.org/";
        wikiLink = new StringItem("Wikipedia\n", url2, 1);
        wikiLink.setFont(Font.getFont(0,0,8));
        wikiLink.addCommand(wikiCommand);
        wikiLink.setItemCommandListener(this);
    }
    
    public Image getImage(String path) {
        Image image = null;
        try {
            image = Image.createImage(path);
        } catch (java.io.IOException exc) {
        }
        return image;
    }
    
    public String getText(String p) {
        InputStream is = this.getClass().getResourceAsStream(p);
        StringBuffer sb = new StringBuffer();
        int ch = 0;
        try {
            if (is != null) {
                while ( (ch = is.read()) != -1) {
                    sb.append((char) ch);
                }
                is.close();
            }
        } catch (java.io.IOException exc) {
            String msg = exc.getMessage();
            mainDisplay.setCurrent(getAlert(msg));
        }
        return sb.toString();
    }
    
    public Alert getAlert(String msg) {
        if (defAlert == null) {
            defAlert = new Alert("Error", msg, null, null);
            defAlert.setType(AlertType.ERROR);
            defAlert.setTimeout(Alert.FOREVER);
        }
        return defAlert;
    }
    
    public void connectionRequest(String adress) {
        try {
            platformRequest(adress);
        } catch (ConnectionNotFoundException exc) {
            String msg = exc.getMessage();
            mainDisplay.setCurrent(getAlert(msg));
        }
    }

    public SplashScreen getSplashScreen() {
        if (splashScreen == null) {
            splashScreen = new SplashScreen(mainDisplay);
            splashScreen.setTimeout(3000);
            splashScreen.setFullScreenMode(true);
            splashScreen.setCommandListener(this);
            splashScreen.setImage(getImage("/splash.png"));
        }
        return splashScreen;
    }
    
    public Form getSearchForm() {
        if (searchForm == null) {
            searchForm = new Form("Search");
            searchForm.append(header);
            searchForm.append(textField);
            searchForm.append(srchButton);
            searchForm.addCommand(exitCommand);
            searchForm.addCommand(srchCommand);
            searchForm.addCommand(helpCommand);
            searchForm.addCommand(clearCommand);
            searchForm.setCommandListener(this);
        }
        return searchForm;
    }
    
    public Form getInformation() {
        if (helpForm == null) {
            helpForm = new Form("Information");
            helpForm.append(info);
            helpForm.append(link);
            helpForm.append(wikiLink);
            helpForm.addCommand(backCommand);
            helpForm.setCommandListener(this);
        }
        return helpForm;
    }
    
    public void startApp() {
        mainDisplay.setCurrent(getSplashScreen());
    }
    
    public void pauseApp() {
        System.gc();
    }
    
    public void destroyApp(boolean unconditional) {
        mainDisplay.setCurrent(null);
    }

    public void commandAction(Command c, Item i) {
        if (c == srchCommand) {
            String s = textField.getString().replace(' ', '+');
            connectionRequest(url1 + "?q=" + s);
        }
        if (c == gotoCommand) {
            connectionRequest(url1);
        }
        if (c == wikiCommand) {
            connectionRequest(url2 + "DuckDuckGo");
        }
    }
    
    public void commandAction(Command c, Displayable d) {
        if (d == searchForm) {
            if (c == exitCommand) {
                destroyApp(true);
                notifyDestroyed();
            } else if (c == helpCommand) {
                mainDisplay.setCurrent(getInformation());
            } else if (c == srchCommand) {
                String s = textField.getString().replace(' ', '+');
                connectionRequest(url1 + "?q=" + s);
                mainDisplay.setCurrent(getSearchForm());
            } else if (c == clearCommand) {
                textField.setString("");
            }
        } else if (d == helpForm) {
            if (c == backCommand) {
                mainDisplay.setCurrent(getSearchForm());
            }
        } else if (d == splashScreen) {
            if (c == SplashScreen.DISMISS_COMMAND) {
                mainDisplay.setCurrent(getSearchForm());
            }
        }
    }

}
```