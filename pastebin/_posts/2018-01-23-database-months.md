---
layout: post
author: Ethicist
title: База данных? Не, не слышал.
description: >
  База данных? Не, не слышал.
categories: pastebin
tags: >
  java j2me source database
---

```java
private final String[] russian = new String[] {
    "Январь",
    "Февраль",
    "Март",
    "Апрель",
    "Май",
    "Июнь",
    "Июль",
    "Август",
    "Сентябрь",
    "Октябрь",
    "Ноябрь",
    "Декабрь"
};

private final String[] english = new String[] {
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "October",
    "November",
    "December"
};

private final String[] ukrainian = new String[] {
    "Cічень",
    "Лютий",
    "Березень",
    "Квітень",
    "Травень",
    "Червень",
    "Липень",
    "Серпень",
    "Вересень",
    "Жовтень",
    "Листопад",
    "Грудень"
};

private final String[] belarusian = new String[] {
    "Студзень",
    "Лютага",
    "Сакавік",
    "Красавік",
    "Май",
    "Чэрвень",
    "Ліпеня",
    "Жнівень",
    "Верасень",
    "Кастрычнік",
    "Лістапада",
    "Снежні"
};

private final String[] polish = new String[] {
    "Styczeń",
    "Luty",
    "Marzec",
    "Kwiecień",
    "Maj",
    "Czerwiec",
    "Lipiec",
    "Sierpień",
    "Wrzesień",
    "Październik",
    "Listopad",
    "Grudzień"
};

private final String[] czech = new String[] {
    "Leden",
    "Únor",
    "Březen",
    "Duben",
    "Květen",
    "Červen",
    "Červenec",
    "Srpen",
    "Září",
    "Říjen",
    "Listopad",
    "Prosinec"
};

private final String[] serbian = new String[] {
    "Јануар",
    "Фебруар",
    "Март",
    "Април",
    "Мај",
    "Jун",
    "Jул",
    "Август",
    "Септембар",
    "Октобар",
    "Новембар",
    "Децембар"
};

private final String[] croatian = new String[] {
    "Siječanj",
    "Veljača",
    "Ožujak",
    "Travanj",
    "Svibanj",
    "Lipanj",
    "Srpanj",
    "Kolovoz",
    "Rujan",
    "Listopad",
    "Studeni",
    "Prosinac"
};

private final String[] lithuanian = new String[] {
    "Sausis",
    "Vasaris",
    "Kovas",
    "Balandis",
    "Gegužės",
    "Birželis",
    "Liepa",
    "Rugpjūtis",
    "Rugsėjis",
    "Spalis",
    "Papkritis",
    "Gruodis"
};

private final String[] finnish = new String[] {
    "Tammikuu",
    "Helmikuu",
    "Maaliskuu",
    "Huhtikuu",
    "Toukokuu",
    "Kesäkuu",
    "Heinäkuu",
    "Elokuu",
    "Syyskuu",
    "Lokakuu",
    "Marraskuu",
    "Joulukuu"
};

private final String[] kazakh = new String[] {
    "Қаңтар",
    "Ақпан",
    "Наурыз",
    "Сәуір",
    "Мамыр",
    "Маусым",
    "Шілде",
    "Тамыз",
    "Қыркүйек",
    "Қазан",
    "Қараша",
    "Желтоқсан"
};

private final String[] turkish = new String[] {
    "Ocak",
    "Şubat",
    "Mart",
    "Nisan",
    "Mayıs",
    "Haziran",
    "Temmuz",
    "Ağustos",
    "Eylül",
    "Ekim",
    "Kasım",
    "Aralık"
};

private final String[] greek = new String[] {
    "Ιανουάριος [Ianouários]",
    "Φεβρουάριος [Fevrouários]",
    "Μάρτιος [Mártios]",
    "Απρίλιος [Aprílios]",
    "Μάιος [Máios]",
    "Ιούνιος [Ioúnios]",
    "Ιούλιος [Ioúlios]",
    "Αύγουστος [Ávgoustos]",
    "Σεπτέμβριος [Septémvrios]",
    "Οκτώβριος [Októvrios]",
    "Νοέμβριος [Noémvrios]",
    "Δεκέμβριος [Dekémvrios]"
};

private final String[] georgian = new String[] {
    "იანვარი [ianvari]",
    "თებერვალი [t’ebervali]",
    "მარტი [marti]",
    "აპრილი [aprili]",
    "მაისი [maisi]",
    "ივნისი [ivnisi]",
    "ივლისი [ivlisi]",
    "აგვისტო [agvisto]",
    "სექტემბერი [sek’temberi]",
    "ოქტომბერი [ok’tomberi]",
    "ნოემბერი [noemberi]",
    "დეკემბერი [dekemberi]"
};

private final String[] chinese = new String[] {
    "一月 [yī yuè]",
    "二月 [èr yuè]",
    "三月 [sān yuè]",
    "四月 [sì yuè]",
    "五月 [wǔ yuè]",
    "六月 [liù yuè]",
    "七月 [qī yuè]",
    "八月 [bā yuè]",
    "九月 [jiǔ yuè]",
    "十月 [shí yuè]",
    "十一月 [shíyī yuè]",
    "十二月 [shí'èr yuè]"
};
```