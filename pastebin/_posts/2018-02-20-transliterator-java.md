---
layout: post
author: Ethicist
title: Транслитерация казахской кириллицы в латиницу.
description: >
  Транслитерация казахской кириллицы в латиницу.
categories: pastebin
tags: >
  java j2me
---

```java
public class Conversion {
    
    public Conversion() { /*...*/ }

    public String latcyr(String original) {
        for (int i = 0; i < lat.length; i++) {
            while (original.indexOf(lat[i]) != -1) {
                original = replace(original, lat[i], cyr[i]);
            }
        }
        return original;
    }

    public String cyrlat(String original) {
        for (int i = 0; i < cyr.length; i++) {
            while (original.indexOf(cyr[i]) != -1) {
                original = replace(original, cyr[i], lat[i]);
            }
        }
        return original;
    }
    
    private String replace(String original, String search, String replaceWith) {
        while (original.indexOf(search) != -1) {
            int start = original.indexOf(search);
            int end = start + search.length();
            original = original.substring(0, start) + replaceWith + original.substring(end);
        }
        return original;
    }
    
    // Kazakh Cyrillic alphabet
    private final String[] cyr = {
        "А", "Ә", "Б", "В", "Г", "Ғ",
        "Д", "Е", "Ё", "Ж", "З", "И",
        "І", "Й", "К", "Қ", "Л", "М",
        "Н", "Ң", "О", "Ө", "П", "Р",
        "С", "Т", "У", "Ұ", "Ү", "Ф",
        "Х", "Ц", "Ч", "Ш", "Щ", "Ъ",
        "Ы", "Ь", "Э", "Ю", "Я",
        "а", "ә", "б", "в", "г", "ғ",
        "д", "е", "ё", "ж", "з", "и",
        "і", "й", "к", "қ", "л", "м",
        "н", "ң", "о", "ө", "п", "р",
        "с", "т", "у", "ұ", "ү", "ф",
        "х", "ц", "ч", "ш", "щ", "ъ",
        "ы", "ь", "э", "ю", "я"
    };
    
    // New Latin script from 2017
    private final String[] lat = {
        "A", "A\'", "B",    "V",      "G",    "G\'", 
        "D", "E",   "I\'o", "J",      "Z",    "I\'", 
        "I", "I\'", "K",    "Q",      "L",    "M", 
        "N", "N\'", "O",    "O\'",    "P",    "R", 
        "S", "T",   "Y\'",  "U",      "U\'",  "F", 
        "H", "S",   "C\'",  "S\'",    "S\'",  "", 
        "Y", "",    "E",    "I\'y\'", "I\'a",
        "a", "a\'", "b",    "v",      "g",    "g\'", 
        "d", "e",   "i\'o", "j",      "z",    "i\'", 
        "i", "i\'", "k",    "q",      "l",    "m", 
        "n", "n\'", "o",    "o\'",    "p",    "r", 
        "s", "t",   "y\'",  "u",      "u\'",  "f", 
        "h", "s",   "c\'",  "s\'",    "s\'",  "", 
        "y", "",    "e",    "i\'y\'", "i\'a"
    };
}
```