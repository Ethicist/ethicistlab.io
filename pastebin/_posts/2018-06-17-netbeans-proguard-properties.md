---
layout: post
author: Ethicist
title: ProGuard obfuscator setting in project.properties
description: >
  ProGuard obfuscator setting in project.properties
categories: pastebin
tags: >
  netbeans ide proguard project properties
---

```
...

obfuscation.custom=-dontusemixedcaseclassnames\n-defaultpackage ''\n-overloadaggressively\n-keep class ** {\n    !private *;\n}

...

-dontusemixedcaseclassnames
-defaultpackage ''
-overloadaggressively
-keep class ** {
    !private *;
}
```