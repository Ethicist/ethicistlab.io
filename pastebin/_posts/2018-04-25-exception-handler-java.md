---
layout: post
author: Ethicist
title: ExceptionHandler.java
description: >
  Универсальный класс для обработки исключений.
categories: pastebin
tags: >
  java j2me exception snippet
---

```java
public class ExceptionHandler extends Exception {

    private Exception thrownException;

    public ExceptionHandler(Exception thrownExc) {
        
        this.thrownException = thrownExc;

        if (thrownException.getClass().isInstance(javax.microedition.io.ConnectionNotFoundException.class)) {
            // DO SOMETHING
        } else if (thrownException.getClass().isInstance(java.io.IOException.class)) {
            // DO SOMETHING
        } else if (thrownException.getClass().isInstance(java.lang.NullPointerException.class)) {
            // DO SOMETHING
        } else if (thrownException.getClass().isInstance(java.lang.OutOfMemoryError.class)) {
            // DO SOMETHING
        } else if (thrownException.getClass().isInstance(java.lang.Exception.class)) {
            // DO SOMETHING
        } else if (thrownException.getClass().isInstance(java.lang.IndexOutOfBoundsException.class)) {
            // DO SOMETHING
        } else if (thrownException.getClass().isInstance(java.lang.ArrayIndexOutOfBoundsException.class)) {
            // DO SOMETHING
        } else if (thrownException.getClass().isInstance(java.lang.ArithmeticException.class)) {
            // DO SOMETHING
        } else if (thrownException.getClass().isInstance(java.lang.IllegalArgumentException.class)) {
            // DO SOMETHING
        } else if (thrownException.getClass().isInstance(java.lang.IllegalStateException.class)) {
            // DO SOMETHING
        } else if (thrownException.getClass().isInstance(javax.microedition.midlet.MIDletStateChangeException.class)) {
            // DO SOMETHING
        } else if (thrownException.getClass().isInstance(javax.microedition.rms.RecordStoreNotFoundException.class)) {
            // DO SOMETHING
        } else if (thrownException.getClass().isInstance(javax.microedition.rms.RecordStoreFullException.class)) {
            // DO SOMETHING
        } else if (thrownException.getClass().isInstance(javax.microedition.rms.RecordStoreException.class)) {
            // DO SOMETHING
        } else if (thrownException.getClass().isInstance(javax.microedition.media.MediaException.class)) {
            // DO SOMETHING
        }

    }
}
```
