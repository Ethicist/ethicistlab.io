---
layout: post
author: Ethicist
title: J2ME Library Dump
description: >
  J2ME Library Dump
categories: pastebin
tags: >
  text database
---

```
/MHT/A Survey of Java ME Today (Update).mht
/MHT/MIDP Event Handling.mht
/MHT/MIDP Event Handling-pf.mht
/MHT/Retrieving MIDlet Attributes.mht
/MHT/pf-The Generic Connection Framework.mht
/MHT/Using Threads in J2ME Applications.mht
/MHT/Databases and MIDP/Databases and MIDP, Part 2 Data Mapping.mht
/MHT/Databases and MIDP/Databases and MIDP, Part 3 Putting Data Mapping to Work.mht
/MHT/Databases and MIDP/Databases and MIDP, Part 4 Filtering and Traversal Strategies.mht
/MHT/Databases and MIDP/Databases and MIDP, Part 5 Searching a Record Store.mht
/MHT/Databases and MIDP/Databases and MIDP, Part 1 Understanding the Record Management System.mht
/MHT/Java net/Java_net - J2ME Tutorial part 2.mht
/MHT/Java net/Java_net - J2ME Tutorial part 3.mht
/MHT/Java net/Java_net - J2ME Tutorial part 4.mht
/MHT/Java net/Accessing a Resource over HTTP.mht
/MHT/Java net/MIDP Network Programming using HTTP and the Connection Framework.mht
/MHT/Java net/The Generic Connection Framework.mht
/MHT/AppSecurity/MIDP Application Security 1 Design Concerns and Cryptography.mht
/MHT/AppSecurity/MIDP Application Security 2 Understanding SSL and TLS.mht
/MHT/AppSecurity/MIDP Application Security 3 Authentication in MIDP.mht
/MHT/AppSecurity/MIDP Application Security 4 Encryption in MIDP.mht
/MHT/AppSecurity/Wireless Java Security.mht
/MHT/AppSecurity/Understanding MIDP 2_0's Security Architecture.mht
/PDF/CLDC-HI_whitepaper-February_2005[1].pdf
/PDF/CodeConventions.pdf
/PDF/langspec-3.0.pdf
/PDF/MIDP_SATSA_PKI_API_Developers_Guide_v1_0_en.pdf
/PDF/Nokia/MIDP_2_0_Game_API_Developers_Guide_v2_0_en.pdf
/PDF/Nokia/MIDP_2_0_Introduction_to_Secure_MIDlet_Communication_v1_0_en.pdf
/PDF/Nokia/MIDP_2_0_Introduction_v1_1_en.pdf
/PDF/Nokia/MIDP_2_0_Signed_MIDlet_Developers_Guide_v2_0_en.pdf
/PDF/Nokia/MIDP_SATSA_Crypto_API_Developers_Guide_v2_0_en.pdf
/PDF/Nokia/NDSforJ2ME_3_0_Users_Guide.pdf
/PDF/Nokia/MIDP_Mobile_3D_Graphics_API_Developers_Guide_v2_0_en.pdf
/PDF/Nokia/Tomi Aarnio - M3G - Java Mobile 3D.pdf
/PDF/basics.pdf
/PDF/book.pdf
/PDF/hussain-makhmud.pdf
/PDF/samouchitel.pdf
/PDF/stepbystep.pdf
/PDF/midp-2_1-mrel-spec.pdf
/PDF/JSR184/m3g5.pdf
/PDF/JSR184/m3g3.pdf
/PDF/JSR184/Claus Hofele - Mobile 3D Graphics 2.pdf
/PDF/JSR184/Morgan Kaufmann Mobile 3D Graphics with OpenGL ES and M3G - Morgan Kaufmann.pdf
/PDF/JSR184/Mobile 3D Graphics 4 (Redikod).pdf
/PDF/JSR184/Mobile 3D Graphics 1 (Redikod).pdf
/PDF/JSR184/Mobile 3D Graphics 2 (Redikod).pdf
/PDF/JSR184/m3g45.pdf
/PDF/JSR184/Mobile 3D Graphics 5 (Redikod).pdf
/PDF/JSR184/Mobile 3D Graphics 3 (Redikod).pdf
/PDF/JSR184/m3g6.pdf
/PDF/JSR184/m3g4.pdf
/PDF/JSR184/m3g2.pdf
/PDF/JSR184/Claus Hofele - Mobile 3D Graphics 1.pdf
/PDF/JSR184/Java Prog. Techniques for Games. M3G Chapter 4.5. M3G File Models.pdf
/PDF/JSR184/Claus Hofele Part 2 - Hello World.pdf
/src/EventEx1.java
/src/EventEx2.java
/src/EventEx3.java
/src/FirstExample.java
/src/SecondExample.java
/src/ThirdExample.java
```