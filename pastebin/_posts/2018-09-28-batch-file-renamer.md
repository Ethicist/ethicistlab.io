---
layout: post
author: Ethicist
title: Рекурсивное переименование в транслит
description: >
  Рекурсивное переименование в транслит
categories: pastebin
tags: >
  bash rename transliteration
---

```bash
#!/bin/bash

rename_files()
{
ls | while read name
	do
		mv -vi "$name" "`echo $name | sed 'y/абвгдезийклмнопрстуфхцыАБВГДЕЗИЙКЛМНОПРСТУФХЦЫ/abvgdezijklmnoprstufxcyABVGDEZIJKLMNOPRSTUFXCY/; s/ш/sh/g; s/ч/ch/g; s/щ/sh'\''/g; s/ё/jo/g; s/ж/zh/g; s/э/je/g; s/ъ/'\''/g; s/ь/'\''/g; s/ю/ju/g; s/я/ja/g; s/Ш/SH/g; s/Ч/CH/g; s/Щ/SH'\''/g; s/Ё/JO/g; s/Ж/ZH/g; s/Э/JE/g; s/Ъ/'\''/g; s/Ь/'\''/g; s/Ю/JU/g; s/Я/JA/g;'`"
	done
find . -maxdepth 1 -mindepth 1 -type d | while read dirname
	do
		cd "$dirname"
		rename_files
		cd ..
	done
}
rename_files
```