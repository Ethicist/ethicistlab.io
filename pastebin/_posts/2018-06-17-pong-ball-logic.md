---
layout: post
author: Ethicist
title: Логика полёта мяча для игры Pong
description: >
  Изобретение велосипедов с продолговатыми колёсами.
categories: pastebin
tags: >
  java j2me pong game ball logic
image:
  path: /pong/pong.jpg
  width: 482
  height: 667
  caption: "Выглядит отвратительно"
  alt: Pong
---

<center>
<br>
{% include media-post-image.html %}
</center>



```java
  void logic() {
    int direction;
    if (ball.get_x() == pad_player.get_x() + 1 || ball.get_x() == pad_player.get_x()) {
      if (ball.get_y() <= pad_player.get_y() + 2 && ball.get_y() >= pad_player.get_y() - 2) {
        if (ball.get_y() >= pad_player.get_y() - 2 && ball.get_y() < pad_player.get_y()) {
          direction = 3; // right-up
        } else if (ball.get_y() <= pad_player.get_y() + 2 && ball.get_y() > pad_player.get_y()) {
          direction = 4; // right-down
        } else {
          direction = 1; // right
        }
      }
    }
    if (ball.get_x() == pad_ai.get_x() - 1 || ball.get_x() == pad_ai.get_x()) {
      if (ball.get_y() <= pad_ai.get_y() + 2 && ball.get_y() >= pad_ai.get_y() - 2) {
        if (ball.get_y() >= pad_ai.get_y() - 2 && ball.get_y() < pad_ai.get_y()) {
          direction = 5; // left-up
        } else if (ball.get_y() <= pad_ai.get_y() + 2 && ball.get_y() > pad_ai.get_y()) {
          direction = 6; // left-down
        } else {
          direction = 2; // left
        }
      }
    }
  }
```