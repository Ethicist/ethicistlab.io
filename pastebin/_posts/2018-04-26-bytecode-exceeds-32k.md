---
layout: post
author: Ethicist
title: Bytecode exceeds 32k
description: >
  Воспроизводим ошибку связанную с ограничением jvm на размер байткода.
categories: pastebin
tags: >
  java j2me error bytecode
---

Для воспроизведения такой ситуации просто создадим обчень большой массив.

Код сокращён. Полная версия: [Application.java]({{ site.url }}/assets/code/Application.java)

```java
import javax.microedition.lcdui.Canvas;
import javax.microedition.lcdui.Display;
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;
import javax.microedition.midlet.*;

public class Application extends MIDlet {

 public void startApp() {
   Display.getDisplay(this).setCurrent(new SplashScreen());
   try {
    Thread.sleep(0x1388L);
   } catch (InterruptedException exc) {
   }
   destroyApp(true);
  }

 public void pauseApp() {}

 public void destroyApp(boolean unconditional) {
  notifyDestroyed();
 }
}

final class SplashScreen extends Canvas {

 public static final int c = 0xb1000d;

 public static final int[] rgb_data = {
 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
 
 /* ... очень много строк кода ...  */
 
 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
};
 
 public static final int image_w = 70;
 public static final int image_h = 100;

 public SplashScreen() {
  setFullScreenMode(true);
 }

 public static Image getImageData() {
  return Image.createRGBImage(rgb_data, image_w, image_h, false);
 }

 public void paint(Graphics g) {
  final int cw = g.getClipWidth();
  final int ch = g.getClipHeight();
  g.setColor(0);
  g.fillRect(0, 0, cw, ch);
  g.drawImage(getImageData(), cw >> 1, ch >> 1, 0x1 | 0x2);
 }
}
```
