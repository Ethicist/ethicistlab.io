---
layout: post
author: Ethicist
title: Первый код на Python. И это не HelloWorld!
description: >
  Первый код на Python. И это не HelloWorld!
categories: pastebin
tags: >
  python
image:
  path: /python/python-first.jpg
  caption: ""
  alt: python
---

<center>
<br>
{% include media-post-image.html %}
</center>

```python
#!/usr/bin/env python
# -*- coding: utf-8 -*-

print()
txt = input("Текст: ")
res = list(txt.upper())

def first(target):
    return target[0]

def last(target):
    return target[-1]

def replace_chars():
    res.append(first(res))
    res.remove(last(res))

def decrease_chars(): 
    del res[-1]

print()

for i in range(len(res)):
    print (' '.join(res))
    replace_chars()

print()

```