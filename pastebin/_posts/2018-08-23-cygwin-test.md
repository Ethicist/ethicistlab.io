---
layout: post
author: Ethicist
title: I'm using WINDOWS!
description: >
  I'm using WINDOWS!
categories: pastebin
tags: >
  cygwin cpp windows test
image:
  path: /cygwin-test/cygwin.jpg
  caption: ""
  alt: cygwin
---

<center>
<br>
{% include media-post-image.html %}
</center>


```cpp
#include <iostream>
#include <cstdlib>
#include <ctime>
#include <stdio.h>
#include <string>

using namespace std;

std::string arr[]= {
	"\033[31m",
	"\033[32m",
	"\033[33m",
	"\033[34m",
	"\033[35m",
	"\033[36m",
	"\033[37m",
	"\033[40m",
	"\033[41m",
	"\033[42m",
	"\033[43m",
	"\033[44m",
	"\033[45m",
	"\033[46m",
	"\033[47m",
};

int main() {
    srand(time(0));
    int r;
    for(int i=0; i<128; i++) {
		    r = rand() % sizeof(arr)/32;
		    std::cout << arr[r];
        std::cout << " I'M USING WINDOWS!!!!! ";
    }
    return 0;
}
```