---
layout: post
author: Ethicist
title: Программа-счётчик
description: >
  Случайная цитата на JavaScript
categories: pastebin
tags: >
  java j2me counter midlet
---

Программа-счётчик.

Application.java

```java

/*
 * Application.java
 */

import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Display;
import javax.microedition.lcdui.Displayable;
import javax.microedition.midlet.MIDlet;

public class Application extends MIDlet {

  Display display = Display.getDisplay(this);
  Command count = new Command("Count", Command.OK, 1);
  Command exit = new Command("Exit", Command.EXIT, 1);
  Command help = new Command("Help", Command.ITEM, 1);
  Counter counter = new Counter(this);

  public void destroyApp(boolean unconditional) {
    notifyDestroyed();
  }

  public void pauseApp() {
    notifyPaused();
  }

  public void startApp() {
    counter.addCommand(help);
    counter.addCommand(exit);
    counter.addCommand(count);
    counter.setCommandListener(new CommandListener() {
      public void commandAction(Command c, Displayable d) {
        if (c == help) {
          counter.show_msg ^= 1;
        } else if (c == count){
          counter.number++;
        } else if (c == exit) {
          destroyApp(true);
        }
      }
    });
    display.setCurrent(counter);
  }
}

```

Counter.java

```java
/*
 * Counter.java
 */

import java.io.IOException;
import javax.microedition.lcdui.Canvas;
import javax.microedition.lcdui.Font;
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;

public class Counter extends Canvas implements Runnable {

  private Application application;
  private Image digits[] = new Image[11];
  private Font font = Font.getFont(Font.FACE_MONOSPACE, Font.STYLE_PLAIN, Font.SIZE_SMALL);
  private int width, height;
  private Image buffer;
  private Graphics g;
  private Thread thread;
  public int number = 0;
  public int show_msg = 1;
  private int img_w = 15;
  private int img_h = 25;

  public Counter(Application app) {
    setFullScreenMode(false);
    this.application = app;
    width = getWidth();
    height = getHeight();
    buffer = Image.createImage(width, height);
    g = buffer.getGraphics();
    try {
      for (int i = 0; i <= 9; i++) {
        digits[i] = Image.createImage("/assets/" + i + ".png");
      }
    } catch (IOException ex) {
    }
  }

  private int sw(String s) {
    return font.stringWidth(s);
  }

  public void drawCounter(Graphics g) {
    int base_x = (g.getClipWidth() / 2) - ((img_w * 8) / 2);
    int base_y = (g.getClipHeight() >> 1) - (img_h >> 1);
    g.setColor(0x2d2d2d);
    g.fillRect(0, 0, width, height);
    for (int i = 0; i < (8 - String.valueOf(number).length()); i++) {
      g.drawImage(digits[0], base_x, base_y, 20);
      base_x += img_w;
    }
    for (int j = 0; j < String.valueOf(number).length(); j++) {
      g.drawImage(digits[Integer.parseInt(("" + number).substring(j, j + 1))], base_x, base_y, 20);
      base_x += img_w;
    }
  }

  public void paint(Graphics g) {
    if (buffer != null) {
      g.drawImage(buffer, 0, 0, 20);
      drawCounter(g);
      if (show_msg == 1) {
        String a = "Counter v0.1";
        String b = "Code by Ethicist";
        String c = "[*] - Show/Hide this text";
        String d = "[0] - Reset counter";
        String e = "[#] - Exit app";
        final int fh = Font.getDefaultFont().getHeight();
        final int bw = g.getClipWidth();
        final int bh = g.getClipHeight();
        g.setFont(font);
        g.setColor(0xffbb00);
        g.drawString(a, (bw>>1)-(sw(a)>>1), fh, 20);
        g.setColor(0xf5f5f5);
        g.drawString(b, (bw>>1)-(sw(b)>>1), fh*2, 20);
        g.drawString(c, (bw>>1)-(sw(c)>>1), bh-fh*4, 20);
        g.drawString(d, (bw>>1)-(sw(d)>>1), bh-fh*3, 20);
        g.drawString(e, (bw>>1)-(sw(e)>>1), bh-fh*2, 20);
      }
    }
    if (thread == null) {
      thread = new Thread(this);
    }
    if (!thread.isAlive()) {
      thread.start();
    }
  }

  public void run() {
    do {
      Runtime runtime = Runtime.getRuntime();
      if (runtime.freeMemory() < 5000L) {
        runtime.gc();
      }
      repaint();
      serviceRepaints();
      byte mls = 20;
      try {
        Thread.sleep((long) mls);
      } catch (InterruptedException e) {
      }
    } while (true);
  }

  protected void keyPressed(int keyCode) {
    if (keyCode == KEY_NUM5 || keyCode == FIRE || keyCode == 53 || keyCode == 8) {
      number++;
    } else if (keyCode == KEY_STAR || keyCode == 42)  {
      show_msg ^= 1;
    } else if (keyCode == KEY_NUM0 || keyCode == 48) {
      number = 0;
    } else if (keyCode == KEY_POUND || keyCode == 35) {
      application.destroyApp(true);
    }
  }
}
```