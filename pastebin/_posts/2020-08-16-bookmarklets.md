---
layout: post
author: Ethicist
title: Несколько полезных букмарклетов на каждый день
description: >
  Коллекция из 13 букмарклетов, которые помогут в работе
categories: pastebin
tags: >
  bookmarklet javascript
---

## Отключение CSS:
```javascript
javascript:void(document.querySelectorAll('style, link[rel=\'stylesheet\']').forEach(s=>s.remove()));
```

## Список ссылок на все картинки со страницы:
```javascript
javascript:r='';i=document.images;for(q=0;q<i.length;q++){if(i[q].width>200){r+=i[q].src+' <br/>'}}rr=window.open();rr.document.write(r);
```

## Список всех ссылок со страницы:
```javascript
javascript:r='';l=document.getElementsByTagName('A');for(q=0;q<l.length;q++){r+=l[q].href+'<br>\n'}rr=window.open();rr.document.write(r);
```

## Просмотреть исходный код страницы:
```javascript
javascript:var n=document.firstChild;while(n && n.nodeType!=Node.ELEMENT_NODE) n=n.nextSibling;document.write('<pre>' + n.outerHTML.replace(/&/g, '&amp;').replace(/</g, '<font color=\'#696\'>&lt;').replace(/>/g, '&gt;</font>'));
```

## Тест адаптивности страницы:
```javascript
javascript:void((function(d){if(self!=top||d.getElementById('toolbar')&&d.getElementById('toolbar').getAttribute('data-resizer'))return false;d.write('<!DOCTYPE HTML><html style="opacity:0;"><head><meta charset="utf-8"/></head><body><a data-viewport="320x480" data-icon="mobile">Mobile (e.g. Apple iPhone)</a><a data-viewport="320x568" data-icon="mobile" data-version="5">Apple iPhone 5</a><a data-viewport="375%C3%97667" data-icon="mobile" data-version="7">Apple iPhone 7</a><a data-viewport="414%C3%97736" data-icon="mobile" data-version="7+">Apple iPhone 7 Plus</a><a data-viewport="600x800" data-icon="small-tablet">Small Tablet</a><a data-viewport="768x1024" data-icon="tablet">Tablet (e.g. Apple iPad 2-3rd, mini)</a><a data-viewport="1280x800" data-icon="notebook">Widescreen</a><a data-viewport="1920%C3%971080" data-icon="tv">HDTV 1080p</a><script src="https://lab.maltewassermann.com/viewport-resizer/resizer.min.js"></script></body></html>')})(document));
```

## Разделить страницу на 2 вкладки:
```javascript
javascript:A14nH=location.href;L3f7=prompt('Choose%20File%201',A14nH);R1Gh7=prompt('Choose%20File%202',L3f7);if(L3f7&&R1Gh7){Fr4Q='<frameset%20cols=\'*,*\'>\n<frame%20src=\''+L3f7+'\'/>';Fr4Q+='<frame%20src=\''+R1Gh7+'\'/>\n';Fr4Q+='</frameset>';with(document){write(Fr4Q);void(close())}}else{void(null)}
```

## Просмотреть robots.txt текущего сайта:
```javascript
javascript:void(window.open(location.protocol + '//' + location.host + '/robots.txt'));
```

## Поиск текущей страницы в Web Archive:
```javascript
javascript:(function(){ window.open('http://web.archive.org/web/*/'+location.href)})();
```

## Открыть страницу в Google Cache:
```javascript
javascript:(function(){window.open('http://webcache.googleusercontent.com/search?q=cache:'+encodeURIComponent(location.href))})();
```

## Проверка сайта на Google PageSpeed Insights:
```javascript
javascript:(function(){window.open('https://developers.google.com/speed/pagespeed/insights/?url='+encodeURIComponent(location.href))})();
```

## Проверка оптимизации для мобильных устройств:
```javascript
javascript:(function(){ window.open('https://www.google.ru/webmasters/tools/mobile-friendly/?url='+encodeURIComponent(location.href))})();
```

## Очистить куки текущего сайта:
```javascript
javascript:(function()%7bC=document.cookie.split(%22; %22);for(d=%22.%22+location.host;d;d=(%22%22+d).substr(1).match(/\..*$/))for(sl=0;sl%3c2;++sl)for(p=%22/%22+location.pathname;p;p=p.substring(0,p.lastIndexOf('/')))for(i in C)if(c=C%5bi%5d)%7bdocument.cookie=c+%22; domain=%22+d.slice(sl)+%22; path=%22+p.slice(1)+%22/%22+%22; expires=%22+new Date((new Date).getTime()-1e11).toGMTString()%7d%7d)();
```

## Сгенерировать пароль:
```javascript
javascript:(function(){var getRandomPass=function(d){var chars='abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';var rez='';var len=d>=3%3Fd:3;while(rez.length<len){rez+=chars.charAt(Math.floor(Math.random()*chars.length))}return/^(%3F=.*%5Cd)(%3F=.*[a-z])(%3F=.*[A-Z]).+$/.test(rez)%3Frez:arguments.callee(len)};var findPassEle=function(p){var c,s='',r=false,f=document.getElementsByTagName('form');for(var i=0;i<f.length;i++){c=f[i].elements;for(var j=0,e;e=c[j];j++){if(e.type=='password'){r=true;if(p){e.value=p;e.focus()}else if(e.value)s+='%5Cn%5Cn'+e.value}}};return r%3Fs:null};var s=findPassEle();if(s){alert('%D0%9D%D0%B0 %D1%8D%D1%82%D0%BE%D0%B9 %D1%81%D1%82%D1%80%D0%B0%D0%BD%D0%B8%D1%86%D0%B5 %D1%83%D0%B6%D0%B5 %D0%B8%D0%BC%D0%B5%D0%B5%D1%82%D1%81%D1%8F %D0%BF%D0%B0%D1%80%D0%BE%D0%BB%D1%8C:'+s)}else{findPassEle(prompt('%D0%9F%D0%B0%D1%80%D0%BE%D0%BB%D1%8C %D1%81%D0%B3%D0%B5%D0%BD%D0%B5%D1%80%D0%B8%D1%80%D0%BE%D0%B2%D0%B0%D0%BD'+(s==null%3F':':'. %D0%92%D1%81%D1%82%D0%B0%D0%B2%D0%B8%D1%82%D1%8C%3F'),getRandomPass(12)))}})();
```