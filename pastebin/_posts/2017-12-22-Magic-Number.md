---
layout: post
author: Ethicist
title: MagicNumber.java
description: >
  MagicNumber.java
categories: pastebin
tags: >
  java j2me source
---

```java
import java.util.Random;

public class MagicNumber {
    
    private Random random = new Random(System.currentTimeMillis());
    private long actionevent[] = new long[32];
    private long randomstore[] = new long[6];
    public MagicNumber instance = new MagicNumber();
    
    public MagicNumber() {
      for(int i=0; i<actionevent.length; i++) {
        actionevent[i] = random.nextLong();
      }
    }

    public long getMagicNumber () {
      long l1 = 0L, l2 = 0L, l3 = 0L, l4 = 0L, l5 = 0L, l6 = 0L;
      l1 = (long)((int)(actionevent[random.nextInt(actionevent.length)] & 0xffffffffL) + random.nextInt()) & 0xffffffffL ^ l1 & 0xffffffff00000000L;
      l2 = (long)actionevent[random.nextInt(actionevent.length)] + (int)(l3 & 0xffffffffL) & 65535L ^ l2 & 0xffffffffffff0000L;
      l3 = (long)(l2 << -351) & 0xffffffffffL ^ l1 & 0xffffffff00000000L;
      l4 = (long)((int)(l1 & 0xffffffffL) + (int)(l3 & 0xffffffffL)) << 16 & 0xffffffff0000L ^ l1 & 0xffff00000000ffffL;
      l5 = (long)actionevent[random.nextInt(actionevent.length)]+(int)((l2 & 0xffffffff0000L) >> 16) & 65535L ^ l3 & 0xffffffffffff0000L;
      if ((char)(int)(l4 & 65535L) != (char)(int)(l3 & 65535L)) {
        l6 = (long)actionevent[random.nextInt(actionevent.length)] << 50 & 0x4000000000000L ^ (long)actionevent[random.nextInt(actionevent.length)] & 0xfffbffffffffffffL;
      } else {
        l6 = random.nextLong();
      }
      randomstore[0] = l1;
      randomstore[1] = l2;
      randomstore[2] = l3;
      randomstore[3] = l4;
      randomstore[4] = l5;
      randomstore[5] = l6;
      return randomstore[random.nextInt(randomstore.length)];
    }
    
    protected void echo(String s) {
      System.out.print(" ["+System.currentTimeMillis()+"] " + s + "\n");
    }

}
```