---
layout: post
author: Ethicist
title: ThreadLoader.java
description: >
  ThreadLoader.java
categories: pastebin
tags: >
  java j2me thread
---


```java
public final class ThreadLoader extends Thread {

  private final Application app;
  
  public ThreadLoader(Application application) {
    this.app = application;
  }
  
  public final void run() {
    try {
      Thread.sleep(3000L);
    } catch (InterruptedException exception) {
      exception.printStackTrace();
    }
  }
}
```

