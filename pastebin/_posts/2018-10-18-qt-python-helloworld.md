---
layout: post
author: Ethicist
title: PyQt4 HelloWorld
description: >
  PyQt4 HelloWorld
categories: pastebin
tags: >
  python qt gui
---

```python
#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys
from PyQt4 import QtGui

def window():
   app = QtGui.QApplication(sys.argv)
   w = QtGui.QWidget()
   b = QtGui.QLabel(w)
   b.setText("Holly shit! o_O")
   w.setGeometry(100,100,200,50)
   b.move(50,20)
   w.show()
   sys.exit(app.exec_())
	
if __name__ == '__main__':
   window()
```