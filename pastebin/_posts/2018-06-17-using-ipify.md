---
layout: post
author: Ethicist
title: Используем публичный IP адрес с Ipify
description: >
  Бредогенератор на Python. Первая версия.
categories: pastebin
tags: >
  python
---

Сайт: [https://www.ipify.org](https://www.ipify.org)

## Python

```python
# This example requires the requests library be installed.  You can learn more
# about the Requests library here: http://docs.python-requests.org/en/latest/
from requests import get

ip = get('https://api.ipify.org').text
print('My public IP address is: {}'.format(ip))
```

## Bash

```bash
#!/bin/bash
ip=$(curl -s https://api.ipify.org)
echo "My public IP address is: $ip"
```
