---
layout: post
author: Ethicist
title: Бредогенератор на Python. Первая версия.
description: >
  Бредогенератор на Python. Первая версия.
categories: pastebin
tags: >
  python
---

Финальная версия: [Бредогенератор](https://gitlab.com/Ethicist/generator)

```python
#!/usr/bin/env python
# -*- coding: utf-8 -*-

import random
from random import choice

print()
num = input(' Количество строк для вывода: ')
num = int(num) if num.isdigit() else 30

first = ['конструкция', 'ремонт', 'монтаж', 'ремонт, наладка и монтаж', 'обслуживание', 'дефектировка', 'классификация', 'технические характеристики', 'технико-экономические показатели', 'основные технические данные', 'назначение', 'способы подключения к трёхфазной сети', 'типовые испытания', 'контрольные испытания', 'условия эксплуатации', 'показатели качества', 'конструктивное исполнение', 'виды дефектов и повреждений']
second = ['электроприводов', 'электроустановок', 'высоковольтных линий электропередач', 'двигателей', 'устройств релейной защиты', 'разъединителей', 'элементов контактной сети', 'воздушных линий', 'глухозаземлённой нейтрали', 'устройств заземления', 'кабельных линий', 'комплектных распределительных устройств (КРУ)', 'комплектных трансформаторных (преобразовательных) установок', 'системы сборных шин', 'трансформаторной подстанции', 'тяговых подстанций', 'щитов управления электроустановками', 'шин и проводов распредустройств', 'магнитных пускателей и контакторов', 'нерегулироемых электроприводов', 'асинхронных двигателей', 'фазного ротора двигателя', 'регулируемых электроприводов', 'коллекторных машин', 'магнитных сердечников асинхронных двигателей', 'генераторов', 'бесконтактного двигателя', 'магнитопроводов в трансформаторах', 'трёхобмоточных трансформаторов', 'опорных тяговых подстанций', 'промежуточных отпаячных подстанций', 'промежуточных транзитных подстанций', 'рабочих контактных подвесок', 'секционных изоляторов', 'спряжений изолированных анкерных участков', 'нейтральных вставок из изолирующих сопряжений', 'однополосных разъединителей с ручным приводом и заземляющим ножом']
last = ['на переменном токе', 'на постоянном токе', 'в зависимости от наличия или отсутствия составных частей']

random.shuffle(first)
random.shuffle(second)
random.shuffle(last)

templates = [
    [first, second, ['а также'], first, second],
    [first, second, ['и'], first, second],
    [first, second, first, second, first, second],
    [['правила технической эксплуатации'], second, ['и'], first, second, last],
    [first, ['и'], first, second, ['а также'], first, second, last],
    [['изучены'], first, ['и'], first, second, ['а также'], first, second, last]
]
 
bullshit = lambda:(' '.join(i for i in map(choice, choice(templates)) if i).capitalize())

print()

for i in range(int(num)):
    print(' 0'+str(i+1)+'.' if i < 9 else ' '+str(i+1)+'.', bullshit())
print()
```