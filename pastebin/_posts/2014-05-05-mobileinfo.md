---
layout: post
author: Ethicist
title: MobileInfo.java
description: >
  MobileInfo.java
categories: pastebin
tags: >
  java j2me mobile info midlet
---

Моё первое более-менее полезное приложение для телефона или _КАК НЕ НАДО ПИСАТЬ КОД_.

Тогда телефоны ещё были с кнопками, и был такой ресурс, Nokia Developer назывался.

```java
import java.io.IOException;
import java.io.InputStream;
import javax.microedition.io.ConnectionNotFoundException;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Display;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Form;
import javax.microedition.lcdui.Image;
import javax.microedition.lcdui.Item;
import javax.microedition.lcdui.ItemCommandListener;
import javax.microedition.lcdui.List;
import javax.microedition.lcdui.StringItem;
import javax.microedition.midlet.MIDlet;
import javax.microedition.rms.RecordStore;
import javax.microedition.rms.RecordStoreException;

public class MobileInfo extends MIDlet 
implements CommandListener, ItemCommandListener
{
  private final Display dsp = getDisplay();
  private Command c01, c02;
  private Command c03, c04;
  private List l01, l02, l03;
  private Form f01, f02, f03;
  private Form f04, f05, f06;
  private Form f07, f08, f09;
  private Form f10, f11, f12;
  private Image image;
  private Image icon;
  
  public void startApp()
  {
    dsp.setCurrent(getList1());
    System.gc();
  }
  
  public void pauseApp()
  {
    System.gc();
  }
  
  public void destroyApp(boolean unconditional)
  {
    System.gc();
    notifyDestroyed();
  }
  
  public void exitMIDlet()
  {
    dsp.setCurrent(null);
    destroyApp(true);
    notifyDestroyed();
  }
  
  public MobileInfo(){
    c01 = new Command("Back", 2, 0);
    c02 = new Command("OK", 8, 1);
    c03 = new Command("OK", 8, 1);
    c04 = new Command("OK", 8, 1);
  }
  
  String[][] array_a = {
    {
      "microedition.platform",
      "microedition.configuration",
      "microedition.profiles",
      "microedition.locale",
      "microedition.encoding",
      "microedition.commports",
      "microedition.hostname",
      "microedition.jtwi.version",
      "microedition.msa.version"
    }, // 0. CLDC, MIDP, JTWI
    {
      "microedition.media.version",
      "microedition.pim.version", 
      "microedition.io.file.FileConnection.version",
      "microedition.m3g.version",
      "microedition.m2g.version",
      "microedition.m2g.svg.version",
      "microedition.m2g.svg.baseProfile",
      "microedition.sip.version",
      "microedition.sensor.version",
      "microedition.location.version",
      "microedition.global.version",
      "microedition.chapi.version",
    }, // 1. Optional Packages
    {
      "audio.encodings",
      "supports.audio.capture",
      "video.encodings",
      "supports.video.capture",
      "video.snapshot.encodings",
      "supports.recording",
      "supports.mixing",
      "streamable.contents"
    }, // 2. Mobile Media API
    {
      "fileconn.dir.memorycard.name",
      "fileconn.dir.memorycard",
      "fileconn.dir.photos.name",
      "fileconn.dir.photos",
      "fileconn.dir.videos.name",
      "fileconn.dir.videos",
      "fileconn.dir.tones.name",
      "fileconn.dir.tones",
      "fileconn.dir.private",
      "file.separator"
    }, // 3. File Connection API
    {
      "bluetooth.api.version",
      "bluetooth.l2cap.receiveMTU.max",
      "bluetooth.connected.devices.max",
      "bluetooth.connected.inquiry",
      "bluetooth.connected.page",
      "bluetooth.connected.inquiry.scan",
      "bluetooth.connected.page.scan",
      "bluetooth.master.switch",
      "bluetooth.sd.trans.max",
      "bluetooth.sd.attr.retrievable.max"
    } // 4. Bluetooth API
  };
  
  String[][] array_b = {
    {
      getText("01.TXT"),
      getText("02.TXT"),
      getText("03.TXT"),
      getText("04.TXT"),
      getText("05.TXT"),
      getText("06.TXT"),
      getText("07.TXT"),
      getText("08.TXT"),
      getText("08.TXT")
    }, // 0. CLDC, MIDP, JTWI
    {
      getText("08.TXT"),
      getText("08.TXT"),
      getText("08.TXT"),
      getText("08.TXT"),
      getText("08.TXT"),
      getText("09.TXT"),
      getText("10.TXT"),
      getText("08.TXT"),
      getText("08.TXT"),
      getText("08.TXT"),
      getText("08.TXT"),
      getText("08.TXT")
    }, // 1. Optional Packages
    {
      getText("11.TXT"),
      getText("12.TXT"),
      getText("11.TXT"),
      getText("12.TXT"),
      getText("11.TXT"),
      getText("12.TXT"),
      getText("13.TXT"),
      getText("14.TXT")
    }, // 2. Mobile Media API
    {
      getText("15.TXT"),
      getText("16.TXT"),
      getText("17.TXT"),
      getText("18.TXT"),
      getText("19.TXT"),
      getText("20.TXT"),
      getText("21.TXT"),
      getText("22.TXT"),
      getText("23.TXT"),
      getText("24.TXT")
    }, // 3. File Connection API
    {
      getText("08.TXT"),
      getText("25.TXT"),
      getText("26.TXT"),
      getText("27.TXT"),
      getText("28.TXT"),
      getText("29.TXT"),
      getText("30.TXT"),
      getText("31.TXT"),
      getText("32.TXT"),
      getText("33.TXT")
    } // 4. Bluetooth API
  };
  
  public void connect(String url){
    try
    {
      platformRequest(url);
    }
    catch (ConnectionNotFoundException exc)
    {
      System.out.print("Unable to connect to " + url);
      System.out.print("Error:" + exc.toString());
      dsp.setCurrent(getForm11());
    }
    dsp.setCurrent(getForm11());
  }
  
  public static String get(String key) {
      return System.getProperty(key);
  }
  
  public long getRMSMemSize() {
    long l = 0L;
    try {
      RecordStore rs = RecordStore.openRecordStore("MI5", true);
      l = rs.getSizeAvailable() + rs.getSize();
      rs.closeRecordStore();
    } catch (RecordStoreException exc) {
      System.out.print("getRMSMemSize()" + exc.toString());
      System.gc();
    }
    return l/1024;
  }
  
  public String getText(String p) {
    InputStream is = this.getClass().getResourceAsStream("/META-RES/" + p);
    StringBuffer sb = new StringBuffer();
    int ch = 0;
    try {
      if (is != null) {
        while ( (ch = is.read()) != -1) {
          sb.append((char) ch);
        }
        is.close();
      }
    } catch (IOException exc) {
      System.out.println("getDescription()" + exc.toString());
      System.gc();
    }
    return sb.toString();
  }
  
  public Image i() {
    if (icon == null) {                                 
      try {                                                
        icon = Image.createImage("/item.png");
      } catch (java.io.IOException exc) {
        System.out.println("getIconImage()" + exc.toString());
        System.gc();
      }
    }                         
    return icon;
  }
  
  public Display getDisplay()
  {
    return Display.getDisplay(this);
  }
  
  public Runtime getRuntime()
  {
      Runtime.getRuntime().gc();
      return Runtime.getRuntime();
  }
  
  public void commandAction(Command c, Item i)
  {
    String url[] = {
      "http://dumpz.org/1058642/",
      "http://developer.nokia.com/",
      "http://spaces.ru/soo/RDmodTEAM"
    };
    if (c == c02) {
      connect(url[0]);
    }
    if (c == c03) {
      connect(url[1]);
    }
    if (c == c04){
      connect(url[2]);
    }
  }
  
  public void commandAction(Command c, Displayable d)
  {
    if (d == f01) {
      if (c == c01)
      {
        dsp.setCurrent(getList2());
      }
    }
    else if (d == f10) {
      if (c == c01)
      {
        dsp.setCurrent(getList3());
      }
    }
    else if (d == f11) {
      if (c == c01)
      {
        dsp.setCurrent(getList1());
      }
    }
    else if (d == f02) {
      if (c == c01)
      {
        dsp.setCurrent(getList2());
      }
    }
    else if (d == f12) {
      if (c == c01)
      {
        dsp.setCurrent(getList1());
      }
    }
    else if (d == f03) {
      if (c == c01)
      {
        dsp.setCurrent(getList2());
      }
    }
    else if (d == f04) {
      if (c == c01)
      {
        dsp.setCurrent(getList2());
      }
    }
    else if (d == f05) {
      if (c == c01)
      {
        dsp.setCurrent(getList2());
      }
    }
    else if (d == f06) {
      if (c == c01)
      {
        dsp.setCurrent(getList3());
      }
    }
    else if (d == f07) {
      if (c == c01)
      {
        dsp.setCurrent(getList3());
      }
    }
    else if (d == f08) {
      if (c == c01)
      {
        dsp.setCurrent(getList3());
      }
    }
    else if (d == f09) {
      if (c == c01)
      {
        dsp.setCurrent(getList3());
      }
    }
    else if (d == l01) {
      if (c == List.SELECT_COMMAND)
      {
        String s = getList1().getString(getList1().getSelectedIndex());
          if (s != null) {
            if (s.equals("Device Info"))
              {
                dsp.setCurrent(getList2());
              }
              else if (s.equals("Memory Info"))
              {
                dsp.setCurrent(getForm12());
              }
              else if (s.equals("Description"))
              {
                dsp.setCurrent(getList3());
              }
              else if (s.equals("About"))
              {
                dsp.setCurrent(getForm11());
              }
              else if (s.equals("Exit"))
              {
                exitMIDlet();
              }
          }
       }
    }
    else if (d == l02) {
      if (c == List.SELECT_COMMAND)
      {
        String s = getList2().getString(getList2().getSelectedIndex());
          if (s != null) {
            if (s.equals("CLDC, MIDP, JTWI"))
            {
              dsp.setCurrent(getForm1());
            }
            else if (s.equals("Optional Packages"))
            {
              dsp.setCurrent(getForm2());
            }
            else if (s.equals("Mobile Media API"))
            {
              dsp.setCurrent(getForm3());
            }
            else if (s.equals("File Connection API"))
            {
              dsp.setCurrent(getForm4());
            }
            else if (s.equals("Bluetooth API"))
            {
              dsp.setCurrent(getForm5());
            }
         }
      }
      else if (c == c01)
      {
        dsp.setCurrent(getList1());
      }
    }
    else if (d == l03) {
      if (c == List.SELECT_COMMAND)
      {
        String s = getList3().getString(getList3().getSelectedIndex());
          if (s != null) {
            if (s.equals("CLDC, MIDP, JTWI"))
            {
              dsp.setCurrent(getForm6());
            }
            else if (s.equals("Optional Packages"))
            {
              dsp.setCurrent(getForm7());
            }
            else if (s.equals("Mobile Media API"))
            {
              dsp.setCurrent(getForm8());
            }
            else if (s.equals("File Connection API"))
            {
              dsp.setCurrent(getForm9());
            }
            else if (s.equals("Bluetooth API"))
            {
              dsp.setCurrent(getForm10());
            }
         }
      }
      else if (c == c01)
      {
        dsp.setCurrent(getList1());
      }
    }
  }
  
  public List getList1()
  {
    if (l01 == null)
    {
      l01 = new List("Mobile Info 5", 3);
      l01.append("Device Info", i());
      l01.append("Memory Info", i());
      l01.append("Description", i());
      l01.append("About", i());
      l01.append("Exit", i());
      l01.setCommandListener(this);
    }
    return l01;
  }
  
  public List getList2()
  {
    if (l02 == null)
    {
      l02 = new List("Device Info", 3);
      l02.append("CLDC, MIDP, JTWI", i());
      l02.append("Optional Packages", i());
      l02.append("Mobile Media API", i());
      l02.append("File Connection API", i());
      l02.append("Bluetooth API", i());
      l02.addCommand(c01);
      l02.setCommandListener(this);
    }
    return l02;
  }
  
  public List getList3()
  {
    if (l03 == null)
    {
      l03 = new List("Description", 3);
      l03.append("CLDC, MIDP, JTWI", i());
      l03.append("Optional Packages", i());
      l03.append("Mobile Media API", i());
      l03.append("File Connection API", i());
      l03.append("Bluetooth API", i());
      l03.addCommand(c01);
      l03.setCommandListener(this);
    }
    return l03;
  }
    
  public Form getForm12()
  {
    long t = getRuntime().totalMemory()/1024;
    long f = getRuntime().freeMemory()/1024;
    if (f12 == null)
    {
      String[] s = {
        "Total RAM: \n", Long.toString(t),
        Long.toString(t*100/t),
        "Free RAM: \n", Long.toString(f),
        Long.toString(f*100/t),
        "Used RAM: \n", Long.toString(t-f),
        Long.toString(100-(f*100/t)),
        "RMS Size: \n",
        Long.toString(getRMSMemSize())
      };
      f12 = new Form("Memoty Info");
      f12.append(new StringItem(s[0], s[1] + " Kb." + " (" + s[2] + "%)"));
      f12.append(new StringItem(s[3], s[4] + " Kb." + " (" + s[5] + "%)"));
      f12.append(new StringItem(s[6], s[7] + " Kb." + " (" + s[8] + "%)"));
      f12.append(new StringItem(s[9], s[10]+ " Kb."));
      f12.addCommand(c01);
      f12.setCommandListener(this);
    }
    return f12;
  }
  
  public Form getForm11()
  {
    if (f11 == null)
    {
      String[] s = {
        "Mobile Info 5 \n",
        "Final Version (EN-US)",
        "Developed by \n",
        "The Running Man",
        "Release date",
        "01.05.2014",
        "Download source:",
        "http://dumpz.org/1058...",
        "Nokia Developer \n",
        "http://developer.nokia...",
        "Modding School \n",
        "http://spaces.ru/soo/RD..."
      };
      f11 = new Form("About");
      StringItem s04 = new StringItem(s[6], s[7], 1);
      StringItem s05 = new StringItem(s[8], s[9], 1);
      StringItem s06 = new StringItem(s[10], s[11], 1);
      f11.append(new StringItem(s[0], s[1]));
      f11.append(new StringItem(s[2], s[3]));
      f11.append(new StringItem(s[4], s[5]));
      f11.append(s04);
      f11.append(s05);
      f11.append(s06);
      s04.addCommand(c02);
      s05.addCommand(c03);
      s06.addCommand(c04);
      f11.addCommand(c01);
      s04.setItemCommandListener(this);
      s05.setItemCommandListener(this);
      s06.setItemCommandListener(this);
      f11.setCommandListener(this);
    }
    return f11;
  }
  
  public Form getForm1()
  {
    if (f01 == null)
    {
      f01 = new Form("CLDC, MIDP, JTWI");
      for(int i = 0; i < array_a.length-4; i++) {
        for(int j = 0; j < array_a[i].length; j++) {
          f01.append(new StringItem(
            array_a[i][j]+"\n", get(array_a[i][j])));
        }
      }
      f01.addCommand(c01);
      f01.setCommandListener(this);
    }
    return f01;
  }
  
  public Form getForm2()
  {
    if (f02 == null)
    {
      f02 = new Form("Optional Packages");
      for(int i = 1; i < array_a.length-3; i++) {
        for(int j = 0; j < array_a[i].length; j++) {
          f02.append(new StringItem(
            array_a[i][j]+"\n", get(array_a[i][j])));
        }
      }
      f02.addCommand(c01);
      f02.setCommandListener(this);
    }
    return f02;
  }
  
  public Form getForm3()
  {
    if (f03 == null)
    {
      f03 = new Form("Mobile Media API");
      for(int i = 2; i < array_a.length-2; i++) {
        for(int j = 0; j < array_a[i].length; j++) {
          f03.append(new StringItem(
            array_a[i][j]+"\n",get(array_a[i][j])));
        }
      }
      f03.addCommand(c01);
      f03.setCommandListener(this);
    }
    return f03;
  }
  
  public Form getForm4()
  {
    if (f04 == null)
    {
      f04 = new Form("File Connection API");
      for(int i = 3; i < array_a.length-1; i++) {
        for(int j = 0; j < array_a[i].length; j++) {
          f04.append(new StringItem(
            array_a[i][j]+"\n", get(array_a[i][j])));
        }
      }
      f04.addCommand(c01);
      f04.setCommandListener(this);
    }
    return f04;
  }
  
  public Form getForm5()
  {
    if (f05 == null)
    {
      f05 = new Form("Bluetooth API");
      for(int i = 4; i < array_a.length; i++) {
        for(int j = 0; j < array_a[i].length; j++) {
          f05.append(new StringItem(
            array_a[i][j]+"\n", get(array_a[i][j])));
        }
      }
      f05.addCommand(c01);
      f05.setCommandListener(this);
    }
    return f05;
  }
  
  public Form getForm6()
  {
    if (f06 == null)
    {
      f06 = new Form("CLDC, MIDP, JTWI");
      for(int i = 0; i < array_b.length-4; i++) {
        for(int j = 0; j < array_b[i].length; j++) {
          f06.append(new StringItem(
            array_a[i][j]+"\n", array_b[i][j]));
        }
      }
      f06.addCommand(c01);
      f06.setCommandListener(this);
    }
    return f06;
  }
  
  public Form getForm7()
  {
    if (f07 == null)
    {
      f07 = new Form("Optiona Packages");
      for(int i = 1; i < array_b.length-3; i++) {
        for(int j = 0; j < array_b[i].length; j++) {
          f07.append(new StringItem(
            array_a[i][j]+"\n", array_b[i][j]));
        }
      }
      f07.addCommand(c01);
      f07.setCommandListener(this);
    }
    return f07;
  }
  
  public Form getForm8()
  {
    if (f08 == null)
    {
      f08 = new Form("Mobile Media API");
      for(int i = 2; i < array_b.length-2; i++) {
        for(int j = 0; j < array_b[i].length; j++) {
          f08.append(new StringItem(
            array_a[i][j]+"\n", array_b[i][j]));
        }
      }
      f08.addCommand(c01);
      f08.setCommandListener(this);
    }
    return f08;
  }
  
  public Form getForm9()
  {
    if (f09 == null)
    {
      f09 = new Form("File Connection API");
      for(int i = 3; i < array_b.length-1; i++) {
        for(int j = 0; j < array_b[i].length; j++) {
          f09.append(new StringItem(
            array_a[i][j]+"\n",array_b[i][j]));
        }
      }
      f09.addCommand(c01);
      f09.setCommandListener(this);
    }
    return f09;
  }
  
  public Form getForm10()
  {
    if (f10 == null)
    {
      f10 = new Form("Bluetooth API");
      for(int i = 4; i < array_b.length; i++) {
        for(int j = 0; j < array_b[i].length; j++) {
          f10.append(new StringItem(
            array_a[i][j]+"\n", array_b[i][j]));
        }
      }
      f10.addCommand(c01);
      f10.setCommandListener(this);
    }
    return f10;
  }
  
}
```