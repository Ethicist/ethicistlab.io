---
layout: post
author: Ethicist
title: Just another switch button
description: >
  Just another CSS3 switch button. No js needed. Wow.
categories: pastebin
tags: >
  css button frontend ui codepen
---

HTML

```html
<label class="switch">
  <input id="" type="checkbox" onchange="">
  <div class="slider round"></div>
</label>
```

CSS

```css
body {
  font-family: system-ui;
  background: #ececec;
  background: -webkit-radial-gradient(#EEE, #CCC);
  background: -moz-radial-gradient(#EEE, #CCC);
  background: -ms-radial-gradient(#EEE, #CCC);
  background: -o-radial-gradient(#EEE, #CCC);
  background: radial-gradient(#EEE, #CCC);
  color: white;
  height: 100vh;
  margin: 0;
  display: grid;
  place-items: center;
}

.switch {
  position: relative;
  display: inline-block;
  width: 50px;
  height: 25px;
}

.switch input {
  opacity: 0;
  width: 0;
  height: 0;
}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background: rgba(0,0,0,0.3);
  -webkit-transition: 0.4s;
  transition: 0.4s;
  border-radius: 50px;
}

.slider:before {
  position: absolute;
  content: "";
  height: 25px;
  width: 25px;
  left: -1px;
  bottom: 0;
  background-color: white;
  -webkit-transition: 0.4s;
  transition: 0.4s;
  border-radius: 50%;
  box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.2),
              0px 0.25px 0.5px rgba(0, 0, 0, 0.2);
}

input:checked + .slider {
  background: rgba(129, 178, 154, 0.75);
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}
```

Result

<p class="codepen" data-height="265" data-theme-id="light" data-default-tab="css,result" data-user="ethicist" data-slug-hash="abdNBWa" style="height: 265px; box-sizing: border-box; display: flex; align-items: center; justify-content: center; border: 2px solid; margin: 1em 0; padding: 1em;" data-pen-title="Switch Button">
  <span>See the Pen <a href="https://codepen.io/ethicist/pen/abdNBWa">
  Switch Button</a> by Captain Anonymous (<a href="https://codepen.io/ethicist">@ethicist</a>)
  on <a href="https://codepen.io">CodePen</a>.</span>
</p>
<script async src="https://static.codepen.io/assets/embed/ei.js"></script>