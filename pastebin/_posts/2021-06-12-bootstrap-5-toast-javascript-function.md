---
layout: post
author: Ethicist
title: Bootstrap 5 Toast JavaScript function
description: >
  Simple function that creates Toast with constructor and shows it with custom delay
categories: pastebin
tags: >
  bootstrap toast javascript
---

Простая функция для создания и отображения Toast. Используется Bootstrap 5 и JavaScript.

### Демо

<p class="codepen" data-height="265" data-theme-id="light" data-default-tab="js,result" data-user="ethicist" data-slug-hash="oNZaWQL" data-preview="true" style="height: 265px; box-sizing: border-box; display: flex; align-items: center; justify-content: center; border: 2px solid; margin: 1em 0; padding: 1em;" data-pen-title="Bootstrap 5 Toast Easy JS function">
  <span>See the Pen <a href="https://codepen.io/ethicist/pen/oNZaWQL">
  Bootstrap 5 Toast Easy JS function</a> by Ethicist (<a href="https://codepen.io/ethicist">@ethicist</a>)
  on <a href="https://codepen.io">CodePen</a>.</span>
</p>
<script async src="https://cpwebassets.codepen.io/assets/embed/ei.js"></script>

### Использование

В параметрах: текст заголовка, текст содержимого, задержка в миллисекундах.

```javascript
show_toast('Hello', 'Lorem ipsum dolor sit amet', 10000);
```

Разумеется, это не идеальное и готовое к использованию решение, но в целом, оно работает, и его легко доработать под нужды проекта.
