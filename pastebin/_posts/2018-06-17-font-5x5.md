---
layout: post
author: Ethicist
title: Пиксельный моноширинный шрифт 5×5
description: >
  Изобретение велосипедов с продолговатыми колёсами.
categories: pastebin
tags: >
  java j2me font canvas game
---

```java
import javax.microedition.lcdui.Canvas;
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;

public class Font_5x5 extends Canvas {

  public static final int char_w = 5; // char width
  public static final int char_h = 5; // char height
  public static final int c = 0x000000; // char color
  public static final int b = 0xffffff; // background

  int[] a = new int[] {
    b, c, c, c, b,
    c, b, b, b, c,
    c, b, b, b, c,
    c, c, c, c, c,
    c, b, b, b, c
  };
  int[] b = new int[] {
    c, c, c, c, b,
    c, b, b, b, c,
    c, c, c, c, b,
    c, b, b, b, c,
    c, c, c, c, c
  };
  int[] c = new int[] {
    c, c, c, c, c,
    c, b, b, b, b,
    c, b, b, b, b,
    c, b, b, b, b,
    c, c, c, c, c
  };
  int[] d = new int[] {
    c, c, c, c, b,
    c, b, b, b, c,
    c, b, b, b, c,
    c, b, b, b, c,
    c, c, c, c, b
  };
  int[] e = new int[] {
    c, c, c, c, c,
    c, b, b, b, b,
    c, c, c, c, b,
    c, b, b, b, b,
    c, c, c, c, c
  };
  int[] f = new int[] {
    c, c, c, c, c,
    c, b, b, b, b,
    c, c, c, c, b,
    c, b, b, b, b,
    c, b, b, b, b
  };
  int[] g = new int[] {
    c, c, c, c, c,
    c, b, b, b, b,
    c, b, b, c, c,
    c, b, b, b, c,
    c, c, c, c, c
  };
  int[] h = new int[] {
    c, b, b, b, c,
    c, b, b, b, c,
    c, c, c, c, c,
    c, b, b, b, c,
    c, b, b, b, c
  };
  int[] i = new int[] {
    c, c, c, c, c,
    b, b, c, b, b,
    b, b, c, b, b,
    b, b, c, b, b,
    c, c, c, c, c
  };
  int[] j = new int[] {
    b, b, b, b, c,
    b, b, b, b, c,
    b, b, b, b, c,
    c, b, b, b, c,
    c, c, c, c, c
  };
  int[] k = new int[] {
    c, b, b, b, c,
    c, b, b, c, b,
    c, c, c, b, b,
    c, b, b, c, b,
    c, b, b, b, c
  };
  int[] l = new int[] {
    c, b, b, b, b,
    c, b, b, b, b,
    c, b, b, b, b,
    c, b, b, b, b,
    c, c, c, c, c
  };
  int[] m = new int[] {
    c, c, b, c, c,
    c, b, c, b, c,
    c, b, c, b, c,
    c, b, c, b, c,
    c, b, b, b, c
  };
  int[] n = new int[] {
    c, b, b, b, c,
    c, c, b, b, c,
    c, b, c, b, c,
    c, b, b, c, c,
    c, b, b, b, c
  };
  int[] o = new int[] {
    c, c, c, c, c,
    c, b, b, b, c,
    c, b, b, b, c,
    c, b, b, b, c,
    c, c, c, c, c
  };
  int[] p = new int[] {
    c, c, c, c, c,
    c, b, b, b, c,
    c, c, c, c, c,
    c, b, b, b, b,
    c, b, b, b, b
  };
  int[] q = new int[] {
    c, c, c, c, c,
    c, b, b, b, c,
    c, b, c, b, c,
    c, b, b, c, b,
    c, c, c, b, c
  };
  int[] r = new int[] {
    c, c, c, c, b,
    c, b, b, b, c,
    c, c, c, c, b,
    c, b, b, b, c,
    c, b, b, b, c
  };
  int[] s = new int[] {
    c, c, c, c, c,
    c, b, b, b, b,
    c, c, c, c, c,
    b, b, b, b, c,
    c, c, c, c, c
  };
  int[] t = new int[] {
    c, c, c, c, c,
    b, b, c, b, b,
    b, b, c, b, b,
    b, b, c, b, b,
    b, b, c, b, b
  };
  int[] u = new int[] {
    c, b, b, b, c,
    c, b, b, b, c,
    c, b, b, b, c,
    c, b, b, b, c,
    c, c, c, c, c
  };
  int[] v = new int[] {
    c, b, b, b, c,
    c, b, b, b, c,
    c, b, b, b, c,
    b, c, b, c, c,
    b, b, c, b, b
  };
  int[] w = new int[] {
    c, b, b, b, c,
    c, b, c, b, c,
    c, b, c, b, c,
    c, b, c, b, c,
    c, c, b, c, c
  };
  int[] x = new int[] {
    c, b, b, b, c,
    b, c, b, c, b,
    b, b, c, b, b,
    b, c, b, c, b,
    c, b, b, b, c
  };
  int[] y = new int[] {
    c, b, b, b, c,
    b, c, b, c, b,
    b, b, c, b, b,
    b, b, c, b, b,
    b, b, c, b, b
  };
  int[] z = new int[] {
    c, c, c, c, c,
    b, b, b, b, c,
    b, c, c, c, b,
    c, b, b, b, b,
    c, c, c, c, c
  };
  int[] char_spacer = new int[] {
    b,
    b,
    b,
    b,
    b
  };
  int[] word_spacer = new int[] {
    b, b, b,
    b, b, b,
    b, b, b,
    b, b, b,
    b, b, b
  };

  public Font_5x5() {
    setFullScreenMode(true);
  }

  public static Image getCharImage(int[] target) {
    return Image.createRGBImage(target, char_w, char_h, false);
  }

  protected void paint(Graphics g) {
  }
  
}
```