---
layout: post
author: Ethicist
title: cue2mp3.py
description: >
  CUE splitter using ffmpeg (to mp3)
categories: pastebin
tags: >
  python mp3 cue converter ffmpeg
---

CUE splitter using ffmpeg (to mp3)

Github Gist: [https://gist.github.com/bancek/b37b780292540ed2d17d](https://gist.github.com/bancek/b37b780292540ed2d17d)

Взял у Luka Zakrajšek, и что-то переписал. 

```python
#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import subprocess
import os

if len(sys.argv) < 2:
    print
    print '    cue2mp3.py'
    print '    CUE splitter using ffmpeg to convert MP3'
    print
    print '    Code by bancek: https://gist.github.com/bancek'
    print '    Modified by Ethicist: https://dumpz.org/aqZGpbsTqkRr'
    print
    print '    USAGE: cue2mp3 [file.cue] [bitrate]'
    print '    EXAMPLE: ~$ python "./cue2mp3.py" "../album/file.cue" 256'
    print
    sys.exit(-1)

cue_file = sys.argv[1]
print

with open(cue_file) as f:
    d = f.read().splitlines()
    track_dir = os.path.dirname(cue_file)
    os.chdir(track_dir)

general = {}

tracks = []

current_file = None

for line in d:
    if line.startswith('REM GENRE '):
        general['genre'] = ' '.join(line.split(' ')[2:])
    elif line.startswith('REM DATE '):
        general['date'] = ' '.join(line.split(' ')[2:])
    elif line.startswith('PERFORMER '):
        general['artist'] = ' '.join(line.split(' ')[1:]).replace('"', '')
    elif line.startswith('TITLE '):
        general['album'] = ' '.join(line.split(' ')[1:]).replace('"', '')
    elif line.startswith('FILE '):
        current_file = ' '.join(line.split(' ')[1:-1]).replace('"', '')
    elif line.startswith('  TRACK '):
        track = general.copy()
        track['track'] = int(line.strip().split(' ')[1], 10)
        tracks.append(track)
    elif line.startswith('    TITLE '):
        tracks[-1]['title'] = ' '.join(line.strip().split(' ')[1:]).replace('"', '')
    elif line.startswith('    PERFORMER '):
        tracks[-1]['artist'] = ' '.join(line.strip().split(' ')[1:]).replace('"', '')
    elif line.startswith('    INDEX 01 '):
        t = map(int, ' '.join(line.strip().split(' ')[2:]).replace('"', '').split(':'))
        tracks[-1]['start'] = 60 * t[0] + t[1] + t[2] / 100.0

for i in range(len(tracks)):
    if i != len(tracks) - 1:
        tracks[i]['duration'] = tracks[i + 1]['start'] - tracks[i]['start']


for track in tracks:
    metadata = {
        'artist': track['artist'],
        'title': track['title'],
        'album': track['album'],
        'track': str(track['track']) + '/' + str(len(tracks))
    }

    if 'genre' in track:
        metadata['genre'] = track['genre']
    if 'date' in track:
        metadata['date'] = track['date']

    cmd = 'ffmpeg'
    cmd += ' -i "%s"' % current_file
    cmd += ' -ss %.2d:%.2d:%.2d' % (track['start'] / 60 / 60, track['start'] / 60 % 60, int(track['start'] % 60))

    if 'duration' in track:
        cmd += ' -t %.2d:%.2d:%.2d' % (track['duration'] / 60 / 60, track['duration'] / 60 % 60, int(track['duration'] % 60))

    if len(sys.argv) > 2:
        cmd += ' -b:a '+sys.argv[2]+'k'
    else:
        cmd += ' -b:a 320k'

    cmd += ' ' + ' '.join('-metadata %s="%s"' % (k, v) for (k, v) in metadata.items())
    cmd += ' "splitted/%.2d - %s - %s.mp3"' % (track['track'], track['artist'], track['title'])

    print ' Executing: '+cmd
    print

    subprocess.call(cmd, shell=True)
```