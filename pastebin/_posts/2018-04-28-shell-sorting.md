---
layout: post
author: Ethicist
title: Сортировка Шелла
description: >
  Метод сортировки Шелла на java.
categories: pastebin
tags: >
  java j2me shell sorting
---

> **Сортировка Шелла** ([англ.](https://ru.wikipedia.org/wiki/Английский_язык)*Shell sort*) — [алгоритм сортировки](https://ru.wikipedia.org/wiki/Алгоритм_сортировки), являющийся усовершенствованным вариантом [сортировки вставками](https://ru.wikipedia.org/wiki/Сортировка_вставками). Идея метода Шелла состоит в сравнении элементов, стоящих не только рядом, но и на определённом расстоянии друг от друга. Иными словами — это сортировка вставками с предварительными «грубыми» проходами. Аналогичный метод усовершенствования [пузырьковой сортировки](https://ru.wikipedia.org/wiki/Сортировка_простыми_обменами) называется [сортировка расчёской](https://ru.wikipedia.org/wiki/Сортировка_расчёской).
>
> [Wikipedia]: https://ru.wikipedia.org/wiki/Сортировка_Шелла	"Сортировка_Шелла"

```java
public void sort(int[] source) {
  int dist = source.length/2 - 1;
  boolean run = true;
  while (run) {
    run = false ;
    for (int i = 0; i < source.length - dist; i++) {
      if (source [i] > source [i + dist]) {
        int local = source[i];
        source[i] = source[i + dist];
        source[i + dist] = local;
        run = true;
        if (dist > 1) dist--;
      }
    }
  }
}
```
