---
layout: post
author: Ethicist
title: PlaylistReader.java
description: >
  PlaylistReader.java
categories: pastebin
tags: >
  java j2me playlist reader
---

Старая версия класса для чтения плейлиста. Плейлист имел собственную структуру:

```
// однострочный комментарий
# ещё один однострочный комментарий
name* = filename
file* = path/to/file.ext
```

где __name__ и __file__ — задаваемые ключи для Hashtable, а __*__ — порядковый номер песни, для сопоставления имени и пути к файлу.

Читаемость такого плейлиста была довольно высокой, но формат имел ряд недостатков:

1. Требовался генератор плейлиста для такого формата.
2. Плейлист несовместим с другими форматами и не открывается другими плеерами.
3. Слишком мало информации по сравнению с форматом m3u.
4. При работе с плейлистом нужно задавать его размер, т.е. количество песен.

В итоге пришлось отказаться от собственного формата и перейти на m3u.

Ниже приведён код старого парсера.

```java
import java.io.InputStream;
import java.io.IOException;
import java.util.Hashtable;

/**
 * @author Ethicist
 */
public class PlaylistReader {

    public String size = "";
    private static Hashtable table;
    private String path;

    public PlaylistReader(String path) {
        this.path = path;
        table = new Hashtable();
        InputStream is = null;
        try {
            is = this.getClass().getResourceAsStream(path);
            boolean done = false;
            byte buffer[] = new byte[1];
            StringBuffer line = new StringBuffer();
            int count = 0;
            while (!done) {
                int bytesRead = is.read(buffer);
                if (bytesRead != -1) {
                    if (buffer[0] == '\n') {
                        String str = line.toString().trim();
                        if (!str.startsWith("#") && !str.startsWith("//")) {
                            if (str.length() > 2 && str.indexOf('=') != -1) {
                                String key = str.substring(0, str.indexOf('=')).trim().toLowerCase();
                                String value = str.substring(str.indexOf('=') + 1).trim();
                                table.put(key, value);
                            }
                        }
                        line.setLength(0);
                    } else {
                        line.append((char) buffer[0]);
                    }
                    count+=1;
                } else {
                    done = true;
                    System.gc();
                }
            }
            size = getFileSizeKiloBytes(count).substring(0, 4) + " kB (" + count + " bytes)";
        } catch (IOException ioe) {
            System.out.println("File " + path + " does not exist.");
            System.out.println("IOException: " + ioe.getMessage());
            ioe.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException ioe) {
                System.out.println("File " + path + " does not exist.");
                System.out.println("IOException: " + ioe.getMessage());
                ioe.printStackTrace();
            }
        }
    }

    private static String getFileSizeKiloBytes(int i) {
        return (double) i/1024 + "";
    }

    public String getString(String key) {
        String result = (String) table.get(key);
        if (result != null) {
            return result;
        }
        return key;
    }
}
```

Пример плейлиста:

```
# 
# MIDI Player playlist file: midiplayer.mpl
# Note: use '#' or "//" to comment the line
# 

playlist_size=25

filename01=Androider
midifile01=/resources/midifiles/track01.mid
filename02=Besatt
midifile02=/resources/midifiles/track02.mid
filename03=Cancertid
midifile03=/resources/midifiles/track03.mid
filename04=Da nu jag och du
midifile04=/resources/midifiles/track04.mid
filename05=Dovten av tomhet
midifile05=/resources/midifiles/track05.mid
```