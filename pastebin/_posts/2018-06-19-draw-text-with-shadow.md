---
layout: post
author: Ethicist
title: Эффект псевдо-тени для текста
description: >
  Эффект псевдо-тени для текста
categories: pastebin
tags: >
  java j2me text shadow effect canvas
---

Работает со стандартными шрифтами телефона. Неплохо выглядит на Nokia и SonyEricsson.

```
final void draw_txt(String s, int bc, int fc, int x, int y, int a) {
  g.setFont(f);
  g.setColor(bc);
  g.drawString(s, x-1, y-1, a);
  g.drawString(s, x-1, y, a);
  g.drawString(s, x-1, y+1, a);
  g.drawString(s, x, y-1, a);
  g.drawString(s, x, y+1, a);
  g.drawString(s, x+1, y-1, a);
  g.drawString(s, x+1, y, a);
  g.drawString(s, x+1, y+1, a);
  g.setColor(fc);
  g.drawString(s, x, y, a);
}

// Только снизу-справа

final void draw_txt(Graphics g, int bc, int fc, String s, int x, int y, int a) {
  g.setFont(f);
  g.setColor(bc);
  g.drawString(s, x+1, y+1, a);
  g.setColor(fc);
  g.drawString(s, x, y, a);
}
```