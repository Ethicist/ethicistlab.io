---
layout: post
author: Ethicist
title: DatabaseLogger.java
description: >
  DatabaseLogger.java
categories: pastebin
tags: >
  java j2me database logger
---

```java
import javax.microedition.rms.RecordListener;
import javax.microedition.rms.RecordStore;
import javax.microedition.rms.RecordStoreException;

/**
 * Just another useless thing we don't ever use
 * @author Ethicist
 */
public class DatabaseLogger implements RecordListener {

    private Application app = new Application();

    public void recordAdded(RecordStore recordstore, int id) {
        try {
            System.out.println("Added record: " + id + "\n");
        } catch (Exception error) {
            String err = "An error occured while adding record:" + "\n";
            String msg = error.getMessage() + "\n" + error.toString();
            System.out.println(err + msg + "\n");
            app.display.setCurrent(app.getAlert(err + msg, 2));
            error.printStackTrace();
        }
    }

    public void recordDeleted(RecordStore recordstore, int id) {
        try {
            System.out.println("Deleted record: " + id + "\n");
        } catch (Exception error) {
            String err = "An error occured while removing record:" + "\n";
            String msg = error.getMessage() + "\n" + error.toString();
            System.out.println(err + msg + "\n");
            app.display.setCurrent(app.getAlert(err + msg, 2));
            error.printStackTrace();
        }
    }

    public void recordChanged(RecordStore recordstore, int id) {
        try {
            System.out.println("Changed record: " + id + "\n");
        } catch (Exception error) {
            String err = "An error occured while changing record:" + "\n";
            String msg = error.getMessage() + "\n" + error.toString();
            System.out.println(err + msg + "\n");
            app.display.setCurrent(app.getAlert(err + msg, 2));
            error.printStackTrace();
        }
    }

    public void displayRecordStoreinfo() throws RecordStoreException {
        RecordStore rs = app.mplayer.settings.database.getRecordStore();
        if (rs == null) {
            try {
                // TODO: This information must be added to separated menu
                rs = RecordStore.openRecordStore("preferences", true);
            } catch (RecordStoreException ex) {
                ex.printStackTrace();
            }
        }
        try {
            System.out.println("Database name: " + rs.getName());
            System.out.println("Total records: " + rs.getNumRecords());
            System.out.println("Database size: " + rs.getSize());
            System.out.println("Available size:" + rs.getSizeAvailable());
            System.out.println("Database version:" + rs.getVersion());
            System.out.println("Last Modified: " + rs.getLastModified());
        } catch (Exception error) {
            String err = "An error occured while displaying DB info:"+"\n";
            String msg = error.getMessage() + "\n" + error.toString();
            System.out.println(err + msg + "\n");
            app.display.setCurrent(app.getAlert(err + msg, 2));
            error.printStackTrace();
        }
    }
}
```