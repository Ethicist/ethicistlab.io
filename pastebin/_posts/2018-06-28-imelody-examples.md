---
layout: post
author: Ethicist
title: Примеры IMY-мелодий
description: >
  Примеры IMY-мелодий
categories: pastebin
tags: >
  imy imelody samples
---

Вертолёт:

```
BEGIN:IMELODY 
VERSION:1.2 
FORMAT:CLASS1.0 
NAME:Helicopter 
COMPOSER:Opelgti 
MELODY:(ledonbackonvibeonr5ledoffbackoffvibeoffr5@0) 
END:IMELODY 
```

Сердце:

```
BEGIN:IMELODY
VERSION:1.2
FORMAT:CLASS1.0
BEAT:400
VOLUME:V15
MELODY:(vibeonr5vibeoffbackoffbackoffbackoffbackoffbackoffbackonledonledonledonledoffr3vibeonr5vibeoffbackoffbackoffbackoffbackoffbackoffbackonledonledonledonledoffr3r2r2r2r2r3@0)
END:IMELODY
```

Nokia Tune:

```
BEGIN:IMELODY
VERSION:1.2
FORMAT:CLASS1.0
NAME:Nokia ringtone
COMPOSER:knallpulver
BEAT:250
STYLE:S0
VOLUME:V15
MELODY:(*5#a3*5#g3*5c2*5d2*5g3*5f3#g2#a2*5f3*5#d3g2#a2*5#d1r0r0r0@3)
END:IMELODY
```

Глюк:

```
BEGIN:IMELODY
VERSION:1.2
FORMAT:CLASS1.0
BEAT:250
MELODY:(vibeonr5ledonbackoffr5vibeoffr5ledoffbackon@0)
END:IMELODY
```

Мегамикс:

```
BEGIN:IMELODY
VERSION:1.2
FORMAT:CLASS1.0
NAME:Megamix 125beat
COMPOSER:Nok2Phone
BEAT:125
STYLE:S1
VOLUME:V7
MELODY:backonvibeon*4b3*4#g3*4e2*4e3*4e3*4e2*4e3*4e3*4e3*4e3*4#d3*4e3*4#f2*4a3*4#f3*4#d2*4#d3*4#d3*4#d2*4#d3*4#d3*4#d3*4#d3*4#c3*4#d3*4e2backoffvibeoffbackonvibeon*5#d3*5d3*5c3*4#a3*5c3*4#d3*4#g3*5c3*4#a3*4#d3*4g3*4f2.backoffvibeoffr2backonvibeon*5#d3*5d3*5c3*4#a3*5c3*4#d3*4#g3*5c3*4#a3*4#d3*4g3*4g2*4f2.*4f3*4g3*4#g3*4#a3*5c3*4#d3*4#g2*5c3*4#a3*4#d3*4g3*4f1.backoffvibeoffr2backonvibeon*4#d3*4f3*4g3*4c3*4#d3*4g3*4#a3*4f3*4#a3*5c1.*5#d3*5d2*5c3*4#a3*5c3*4#d3*4#g3*5c3*4#a3*4#d3*4g3*4f2.backoffvibeoffr2backonvibeon*4b3*5#c2*5e2backoffvibeoffr2backonvibeon*4b2*5d2backoffvibeoffr2backonvibeon*5d2*5d2*5e2*5c2*4b2*4a2*4e2*4a2*4b2*4g2*4#f2*4e2backoffvibeoffr2backonvibeon*4b2*5e2*5e2*5e2*5d2backoffvibeoffr2backonvibeon*5d2*5d2*5e2*5e2*5c2*4b2*4a2backoffvibeoffr2backonvibeon*4a2*4b2*4g2*4#f2*4e2backoffvibeoff*5f3r4*5f4*5c4r4*5f1r3*4#g3*4a2*5c3*4f2r3*4a4*5c4*5f3*5c3*5g3*5f2r2r3*5d3r4*5d4*5c4*4a3r4*5c2r2backonvibeon*4#g3*4e3*4#g3*5#c3*4a2backoffvibeoffr2backonvibeon*4#f3*4#d3*4#f3*4b3*4#g2*4#f3*4e3backoffvibeoffr2backonvibeon*4e3*4#c3*4#f2*4#c2backoffvibeoffr2backonvibeon*4#f3*4e3*4#g2*4#f2backoffvibeoffbackonvibeon*4f4*4e4*4f4*4e4*4f4*4e4*4d3*4e4*4d4*4e4*4d4*4e4*4d4*4c4*4d4*4d2backoffvibeoffbackonvibeon*5#f3*5#f3*5#f3*5#f3*5g2*5#f3*5e2*5e3*5a3*5#f2*5d2backoffvibeoffr1r2r3backonvibeon*4b4*4a4*4b2*4e2backoffvibeoffr2r3backonvibeon*5c4*4b4*5c3*4b3*4a2backoffvibeoffr2r3backonvibeon*5c4*4b4*5c2*4e2backoffvibeoffr2r3backonvibeon*4a4*4g4*4a3*4g3*4#f3*4a3*4g2.*4#f4*4g4*4a2.*4g4*4a4*4b3*4a3*4g3*4#f3*4e2*5c2*4b1.*4b4*5c4*4b4*4a4*4b0backoffvibeoffbackonvibeon*4e3*4a2.*4e3*5e2.*4a3*5#c2*4b3*5#c3*4a2*4e2*4a2*5#f2*5e2*5#c3*5d3*5e1.backoffvibeoffr3backonvibeon*4e3*4a2*5#f2*5e2*5#c3*5d3*5e2*4b3*5#c3*4a2*4e2*4a2*5#c3*5d3*4b2.*4a3*4a1backoffvibeoffbackonvibeon*4f4*4e4*4f4*4e4*4f4*4e4*4d3*4e4*4d4*4e4*4d4*4e4*4d4*4c4*4d4*4d2backoffvibeoffbackonvibeon*4b3*5c3*5d3*5#c3*5d3*5#c3*5d3*5g3*5e2.*5d3*5c3*4b3*5c3*4b3*5c3*4b3*5c3*5#f3*5d2.*5c3*4b3*4a3*4g3*4#f3*4g3*4#f3*4g3*5d3*5c3*4b3*5c3*4b3*4a3*4g3*4#f3*4g3*4e3*4#f3*4d2*5c3*5d3*4b3*5c3*4a2backoffvibeoffbackonvibeon*4g3.*4g3.*4g3*4c3*4c3*4d3*4d3*4g3.*4g3.*4g3*4#a3*4#a3*5c3*5c3*4g3.*4g3.*4g3*4c3*4c3*4d3*4d3*4g3.*4g3.*4g3*4#a3*4#a3*5c3*5d3backoffvibeoffr1r2backonvibeon*4b3*4a3.*4#f4*4e3backoffvibeoffr2r3backonvibeon*4#f3*4#g3*4a2*4a3*4a3*4a2*4#c3*4d3*4e1backoffvibeoffr3backonvibeon*4#f3*4#g3*4e1backoffvibeoffbackonvibeon*4e4backoffvibeoffr4backonvibeon*4f4*4e4*4e4backoffvibeoffr4backonvibeon*4e4*4e4*4f4*4e4*4e4*4e4*4#d4*4e4*4e4*4e4*4e4backoffvibeoffr4backonvibeon*4f4*4e4*4e4backoffvibeoffr4backonvibeon*4f4*4e4*4f4*4e4*4e4*4e4*4#d4*4e4*4e4*4e4*4d4backoffvibeoffr4backonvibeon*4e4*4d4*4d4backoffvibeoffr4backonvibeon*4e4*4d4*4e4*4d4*4d4*4d4*4c4*4d4*4d4*4d4*4d4backoffvibeoffr4backonvibeon*4e4*4d4*4d4backoffvibeoffr4backonvibeon*4e4*4d4*4e4*4d4*4d4*4d4*4c4*4d4*4d4*4d4backoffvibeoffbackonvibeon*4a3*4a3*4a3*4a2*4e3*4d3backoffvibeoffr3backonvibeon*4c3backoffvibeoffr3backonvibeon*4d3backoffvibeoffr3backonvibeon*4e3*4d2*4e3*4d3*4c3*4a3*4a3*4a2*4e3*4d3backoffvibeoffr3backonvibeon*4c3backoffvibeoffr3backonvibeon*4d3backoffvibeoffr3backonvibeon*4e3*4e3*4e3*4d3*4c2*4a3*4a3*4a2*4e3*4d3backoffvibeoffr3backonvibeon*4c3backoffvibeoffr3backonvibeon*4d3backoffvibeoffr3backonvibeon*4e3*4e3*4e3*4d3*4c3*4a3*4a3*4a2*4e3*4e3*4e3*4d3*4c3*4d3*4e3backoffvibeoffbackonvibeon*4e2*4b2*4a2*4b2*5d2*4b1.backoffvibeoffr0backonvibeon*4e2*4b2*4a2*4b2*5e2*4b1.backoffvibeoffr0backonvibeon*5g2*5#f2*5e2*5d2*5e2*4b1.backoffvibeoffr0backonvibeon*5g2*5#f2*5e2*5d2*5#f2*4b1.backoffvibeoffr0backonvibeon*4e2*4b2*4a2*4b2*5d2*4b1.backoffvibeoffr0backonvibeon*4e2*4b2*4a2*4b2*5e2*4b1.backoffvibeoffr0backonvibeon*5e2*4b1.backoffvibeoffbackonvibeon*5#c4*5d1backoffvibeoffr1backonvibeon*5#c4*5d1backoffvibeoffr1backonvibeon*5e5*5d5*5#c5*5d1backoffvibeoffr1backonvibeon*5#c4*5d1backoffvibeoffr1backonvibeon*4b4*5c1backoffvibeoffr1backonvibeon*5d5*5c5*4b5*5c1backoffvibeoffr1backonvibeon*4#a4*4b1backoffvibeoffr2r3backonvibeon*5c5*4b5*4a5*4g5*4b5*4a1backoffvibeoffr1backonvibeon*4a5*4g5*4#f5*4a5*4g0backoffvibeoffr0backonvibeon*5#c3*5d3*5d3*5d3*5d3*5d3*5d3*5d3*5#c3*5d3*5d3*5d3*5d3*5d3*5e4*5d4*5#c4*5e4*5#c3*5d3*5d3*5d3*5d3*5d3*5d3*5d3*5#c3*5d3*5d3*5d3*5d3*5d3backoffvibeoff
END:IMELODY
```