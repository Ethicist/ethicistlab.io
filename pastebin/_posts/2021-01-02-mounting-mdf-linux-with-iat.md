---
layout: post
author: Ethicist
title: "Монтирование MDF в Debian и конвертирование MDF в ISO"
description: >
  Пример использования утилиты для тестирования образов Iso9660 Analyzer Tool
categories: pastebin
tags: >
  debian software
---
# Монтирование MDF

```bash
# mkdir -p /mnt/mdf/
# mount -o loop -t iso9660 {file}.mdf /mnt/mdf
```

## Установка Iso9660 Analyzer Tool

```bash
$ wget -O iat-0.1.7.tar.gz https://deac-riga.dl.sourceforge.net/project/iat.berlios/iat-0.1.7.tar.gz
$ tar -xvzf iat-0.1.7.tar.gz
$ cd iat-0.1.7
$ ./configure && make && make install
```

## Конвертирование MDF в ISO

```bash
# iat --iso -i /path/to/file.mdf -o /path/to/file.iso
```