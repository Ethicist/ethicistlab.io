---
layout: post
author: Ethicist
title: Проверка состояния сети
description: >
  Проверка состояния сети
categories: pastebin
tags: >
  bash ping networking
---

```bash
#/bin/bash

check_networking() {
  echo "	" $(date +%H:%M:%S) "	Проверка соединения..."
  ping -c 3 anus.com > /dev/null
  if [ $? = "0" ];then
    echo "	" $(date +%H:%M:%S) "	Интернеты есть."
  else
    echo "	" $(date +%H:%M:%S) "	Интернетов нет."
  fi
  sleep 1
}

check_networking
```
