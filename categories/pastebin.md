---
layout: default
title: Pastebin
permalink: /pastebin/
pagination: 
  enabled: true
  category: pastebin
  permalink: /:num/
---

<div class="pagination">
  {% if paginator.previous_page %}
    <span class="prev">
      <a href="{{ site.baseurl }}{{ paginator.previous_page_path }}" class="prev">
        ← сюда
      </a>
    </span>
  {% endif %}
  {% if paginator.next_page %}
    <span class="next">
      <a href="{{ site.baseurl}}{{ paginator.next_page_path }}" class="next">
        туда →
      </a>
    </span>
  {% endif %}
</div>
