package constants;

public class CPortingValues {

public static final int ROAD_PADDING = 5;
public static final int TOUCH_PADDING = 4;
public static final int LOGO_TIME = 1;
public static final int SAVE_ME_TIME = 5;
public static final int POLICE_CAR_STAY_TIME = 5;
public static final int NOTIFIRE_TIME = 3;
public static final int HEAD_START_DISTANCE_FACTOR = 100;
public static final int BIKE_START_SPEED = 10;
public static final int BIKE_SPEED_LIMIT = 20;
public static final int HEALTH_CAR = 200;
public static final int HEALTH_OBSTACLE = 100;
public static final int DAMAGE_BULLETTE = 50;
public static final int DAMAGE_MISSILE = 100;
public static final int ROAD_CONTINUE_MAX = 15;
public static final int ROAD_CONTINUE_MIN = 5;
public static final int TOTAL_LEVEL_DATA = 15;
public static final int LEVEL_DIST_INCRSE = 5000;
public static final int OBSTACLE_GAP = 400;
public static final int BULLET_SPEED = 40;
public static final int MISSILE_SPEED = 20;
public static final int HUD_COIN_X = 215;
public static final int HUD_COIN_Y = 71;
public static final int HUD_DISTANCE_X = 50;
public static final int HUD_DISTANCE_Y = 3;
public static final int HUD_AMMO_X = 64;
public static final int HUD_AMMO_Y = 18;
public static final int HUD_AMMO_IMG_X = 27;
public static final int HUD_AMMO_IMG_Y = 13;
public static final int NOTIFIRE_X = 80;
public static final int iMaxLabelsOnMenu = 4;
public static final int iMaxLabelsOnOptionMenu = 3;
public static final int LEFT_RIGHT_BASE_SPEED = 8;
public static final int LOGO_ANIMATION_SPEED = 5;
public static final int MAX_IN_APP_PURCHASE = 5;
public static final int CAR_GEN_GAP = 500;
public static final int OBSTCL_GEN_GAP = 500;
public static final int TOTAL_BORDER_IMAGES = 10;
public static final int TOTAL_BORDER_IMAGES_WITHOUTFLIP = 6;
public static final int SK_HEIGHT = 25;
public static final int TEXT_Y_MOVEMENT = 10;
public static final int THE_GAME_HEIGHT = 3;
public static final int iTotalTopScores = 10;
public static final int COST_SAVE_ME = 100;
public static final int COST_BULLET_BIKE = 200;
public static final int COST_MISSILE_BIKE = 500;
public static final int COST_BULLET = 200;
public static final int COST_MISSILE = 300;
public static final int AMOUNT_BULLET_ADD_BY = 10;
public static final int AMOUNT_MISSILE_ADD_BY = 5;
public static final int ARROW_Y_MOVEMENT = 5;
public static final int COIN_ATTRACT_SPEED = 5;
public static final int CHOPPER_SPEED = 3;
public static final int COUNT_CONTROL_POPUP_SHOW = 3;

public static int COST_MAGNET[] = { 30, 60, 120, 240, 480 };
public static int COST_SHIELD[] = { 50, 100, 200, 400, 800 };
public static int AMOUNT_SHIELD_TIME[] = { 5, 7, 9, 11, 13, 15 };
public static int AMOUNT_MAGNET_TIME[] = { 5, 7, 9, 11, 13, 15 };

public static final String ZONE_ID = "efb98675";

public static int LEVEL_UP_DIST[] = { 1000, 2000, 3000, 5000, 7000, 
9000, 12000, 15000, 18000, 22000, 26000, 30000, 35000, 40000, 50000
};


}