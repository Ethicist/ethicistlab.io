// Минимальное значение громкости - на этом уровне идёт отключение звука
var EPSILON = 0.001;

// Коэффициент для преобразований в dBFS и обратно
var DBFS_COEF = 20 / Math.log(10);

// По положению на шкале вычисляет громкость
var volumeToExponent = function(value) {
	var volume = Math.pow(EPSILON, 1 - value);
	return volume > EPSILON ? volume : 0;
};

// По значению громкости вычисляет положение на шкале
var volumeFromExponent = function(volume) {
	return 1 - Math.log(Math.max(volume, EPSILON)) / Math.log(EPSILON);
};

// Перевод значения громкости в dBFS
var volumeToDBFS = function(volume) {
	return Math.log(volume) * DBFS_COEF;
};

// Перевод значения dBFS в громкость
var volumeFromDBFS = function(dbfs) {
	return Math.exp(dbfs / DBFS_COEF);
}